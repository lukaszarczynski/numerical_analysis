// Łukasz Arczyński
// nr. indeksu 258424
// zad P2.7
// Wrocław, 14 XII 2013 r.

#include <iostream>
#include <cstdlib>
#include <iomanip>
#include "polynomial.h"

using namespace std;

///// Ustalanie wspolczynnikow A, B, C lub alpha, beta, gamma /////

void userDefinedPolynomials(int degree, double *A, double *B, double *C, string a, string b, string c)
{  // wspolczynniki losowe lub wpisane przez uzytkownika
    char equal, random;
    cout << "Czy wszystkie wspolczynniki " << a << "_k sa rowne, " << b << "_k sa rowne i " << c <<
    "_k\nsa rowne (k=1,...,n)? (t/n)\n";
    cin >> equal;
    cout << "Losowac " << a << "_k, " << b << "_k, " << c << "_k? (t/n)\n";
    cin >> random;
    cout << "Podaj " << a << "_0, " << b << "_0:\n";
    cin >> A[0] >> B[0];
    if (equal == 't')
    {
        double A_k, B_k, C_k;
        if (random == 'n')
        {
            cout << "Podaj " << a << "_k, " << b << "_k, " << c << "_k:\n";
            cin >> A_k >> B_k >> C_k;
        }
        if (random == 't')
        {
            A_k = (double) rand() / (RAND_MAX) * 10;
            B_k = (double) rand() / (RAND_MAX) * 10;
            C_k = (double) rand() / (RAND_MAX) * 10;
        }
        for (int i = 1; i < degree + 1; i++)
        {
            A[i] = A_k, B[i] = B_k;
            C[i] = C_k;
        }
    }
    if (equal == 'n')
    {
        for (int i = 1; i < degree + 1; i++)
        {
            if (random == 't')
            {
                A[i] = (double) rand() / (RAND_MAX) * 10;
                B[i] = (double) rand() / (RAND_MAX) * 10;
                C[i] = (double) rand() / (RAND_MAX) * 10;
            }
            if (random == 'n')
            {
                cout << "Podaj " << a << "_" << i << ", " << b << "_" << i << ", " << c << "_" << i << ":\n";
                cin >> A[i] >> B[i] >> C[i];
            }
        }
    }
    A[degree + 1] = A[degree];
    C[degree + 1] = C[degree];
}

void polynomialsWithConstantRecursiveEquation(int degree, double *A, double *B, double *C, double A0, double B0,
                                              double Ak, double Bk, double Ck)  // stale wspolczynniki
{
    A[0] = A0;
    B[0] = B0;
    for (int i = 1; i < degree + 1; i++)
    {
        A[i] = Ak, B[i] = Bk;
        C[i] = Ck;
    }
}

void hermitePolynomials(int degree, double *A, double *B, double *C) // wspolczynniki wielomianow Hermite'a
{
    A[0] = 2;
    B[0] = 0;
    for (int i = 1; i < degree + 1; i++)
    {
        A[i] = 2, B[i] = 0;
        C[i] = 2 * i;
    }
}

void legendrePolynomials(int degree, double *A, double *B, double *C) // wspolczynniki wielomianow Legendre'a
{
    A[0] = 1;
    B[0] = 0;
    for (int i = 1.0; i < degree + 1; i++)
    {
        A[i] = (2.0 * i + 1.0) / (i + 1.0), B[i] = 0;
        C[i] = i / (i + 1.0);
    }
}

void laguerrePolynomials(int degree, double *A, double *B, double *C) // wspolczynniki wielomianow Laguerre'a
{
    A[0] = 1;
    B[0] = -1;
    for (int i = 1.0; i < degree + 1; i++)
    {
        A[i] = (2.0 * i + 1.0) / (i + 1.0), B[i] = -1.0 / (i + 1.0);
        C[i] = i / (i + 1.0);
    }
}

void gegenbauerPolynomials(int degree, double *A, double *B, double *C, double lambda) // wspolczynniki wielomianow Gegenbauera
{
    A[0] = 2.0 * lambda;
    B[0] = 0;
    for (int i = 1.0; i < degree + 1; i++)
    {
        A[i] = 2.0 * (i + lambda) / (i + 1.0), B[i] = 0;
        C[i] = (i + 2.0 * lambda - 1.0) / (i + 1.0);
    }
}

void jacobiPolynomials(int degree, double *A, double *B, double *C, double alpha, double beta) // wspolczynniki wielomianow Jacobiego
{
    A[0] = (alpha + beta + 2.0) / 2.0;
    B[0] = (beta - alpha) / 2.0;
    for (int i = 1.0; i < degree + 1; i++)
    {
        A[i] = ((2.0 * i + alpha + beta + 1.0) * (2.0 * i + alpha + beta + 2.0)) /
               ((2.0 * i + 2.0) * (i + alpha + beta + 1.0));
        B[i] = ((2.0 * i + alpha + beta + 1.0) * (alpha * alpha - beta * beta)) /
               ((2.0 * i + 2.0) * (i + alpha + beta + 1.0) * (2.0 * i + alpha + beta));
        C[i] = (2.0 * (i + alpha) * (i + beta) * (2.0 * i + alpha + beta)) /
               ((2.0 * i + 2.0) * (i + alpha + beta + 1.0) * (2.0 * i + alpha + beta));
    }
}

void selectPolynomialsType(int degree, double *A, double *B, double *C, string nameOfA, string nameOfB, string nameOfC, string nameOfPolynomial)
{ // wybor, ktorymi z powyzszych funkcji okreslone beda wspolczynniki A, B, C lub alpha, beta, gamma
    int typ;
    cout << "Jakimi wielomianami maja byc wielomiany " << nameOfPolynomial <<
            "_n?\n0. x^n\n1. Czebyszewa 1. rodzaju\n2. Czebyszewa 2. rodzaju\n"
            "3. Hermite'a\n4. Legendre'a\n5. Laguerre'a\n6. Gegenbauera z dana wartoscia lambda\n"
            "7. Jacobiego z danymi wartosciami alpha i beta\n8. Innymi\n";
    cin >> typ;
    switch (typ)
    {
        case 0:
            polynomialsWithConstantRecursiveEquation(degree, A, B, C, 1, 0, 1, 0, 0);
            break;
        case 1:
            polynomialsWithConstantRecursiveEquation(degree, A, B, C, 1, 0, 2, 0, 1);
            break;
        case 2:
            polynomialsWithConstantRecursiveEquation(degree, A, B, C, 2, 0, 2, 0, 1);
            break;
        case 3:
            hermitePolynomials(degree, A, B, C);
            break;
        case 4:
            legendrePolynomials(degree, A, B, C);
            break;
        case 5:
            laguerrePolynomials(degree, A, B, C);
            break;
        case 6:
            double lambda;
            cout << "Podaj wartosc lambda\n";
            cin >> lambda;
            gegenbauerPolynomials(degree, A, B, C, lambda);
            break;
        case 7:
            double alphaJ, betaJ;
            cout << "Podaj wartosc alpha i beta\n";
            cin >> alphaJ >> betaJ;
            jacobiPolynomials(degree, A, B, C, alphaJ, betaJ);
            break;
        case 8:
            userDefinedPolynomials(degree, A, B, C, nameOfA, nameOfB, nameOfC);
            break;
        default:
            userDefinedPolynomials(degree, A, B, C, nameOfA, nameOfB, nameOfC);
            break;
    }
    A[degree + 1] = A[degree];
    C[degree + 1] = C[degree];
}

///// Obliczanie kolejnych wielomianow ortogonalnych z wspolczynnikami A, B, C, lub alpha, beta, gamma /////

void orthogonalPolynomials(int degree, Polynomial *w, double *A, double *B, double *C)
{
    double w0[] = {1};
    double x[] = {0, 1};
    Polynomial X(1, x);
    w[0] = Polynomial(0, w0);
    w[1] = (A[0] * X * w[0]) + B[0] * w[0];
    for (int i = 1; i < degree; i++)
        w[i + 1] = (A[i] * X * w[i]) + B[i] * w[i] - C[i] * w[i - 1];
}

///// wypisywanie danego wielomianu i wielomianow, za pomoca ktorych zostanie on przedstawiony /////

void printData(int degree, double *d, Polynomial *q, Polynomial *Q, Polynomial &f_n)
{
    for (int i = 1; i <= degree; i++)
        f_n += d[i] * Q[i];
    cout << "Wielomian f_n = \n";
    for (int i = degree; i > 0; i--)
        cout << d[i] << "*(" << Q[i] << ") + ";
    cout << d[0] << " = \n" << f_n;
    cout << "\nzostanie przedstawiony za pomoca kombinacji liniowej wielomianow\n";
    for (int i = 0; i <= degree; i++)
        cout << q[i] << "\n";
}

///// obliczanie wartosci wspolczynnikow e_k /////

void changePolynomialBase(int degree, double *d, double *e, double *A, double *B, double *C,
                          double *alpha, double *beta, double *gamma)
{
    double g[degree + 2][degree + 2];
    for (int n = 0; n < degree + 2; n++)
        for (int k = 0; k < degree + 2; k++)
            g[k][n] = 0;
    g[0][0] = 1.0;
    g[0][1] = beta[0] - (alpha[0] * B[0] / A[0]);
    g[1][1] = alpha[0] / A[0];
    for (int n = 2; n < degree + 1; n++)
        for (int k = 0; k <= n; k++)
        {
            g[k][n] = (alpha[n - 1] * C[k + 1] / A[k + 1]) * g[k + 1][n - 1] -
                      ((alpha[n - 1] * B[k] / A[k]) - beta[n - 1]) * g[k][n - 1] - gamma[n - 1] * g[k][n - 2];
            if (k > 0)
                g[k][n] += (alpha[n - 1] / A[k - 1]) * g[k - 1][n - 1];
        }
    for (int i = 0; i < degree + 1; i++)
        e[i] = 0.0;
    for (int k = 0; k < degree + 1; k++)
        for (int s = k; s < degree + 1; s++)
            e[k] += d[s] * g[k][s];
}

///// wypisywanie znalezionej kombinacji liniowej /////

void printResult(int degree, double *e, Polynomial *q, Polynomial f_n)
{
    cout << "Znalezione przedstawienie:\nf_n = ";
    for (int i = degree; i > 0; i--)
        cout << e[i] << "*(" << q[i] << ") + ";
    cout << e[0] << " * (" << q[0] << ") = ";
    Polynomial L_n = e[0] * q[0];
    for (int i = 1; i <= degree; i++)
        L_n += e[i] * q[i];
    cout << L_n << "\n";
    cout << "\nBlad przedstawienia: " << f_n - L_n << "\n";
}

///// zaburzanie wspolczynnikow d_k w celu zbadania uwarunkowania zadania /////

void disruptCoefficients(int degree, double *coefficients, double *disruptedCoefficients)
{
    for (int i = 0; i <= degree; i++)
        disruptedCoefficients[i] = 1.1 * coefficients[i] - (double) rand() / (RAND_MAX) * 0.2 * coefficients[i];
}

void disrupt(int degree, double *d, double *A, double *B, double *C, double *alpha, double *beta, double *gamma,
             double *e, Polynomial *Q, Polynomial *q, Polynomial f_n)
{
    char disruption;
    cout << "\nCzy zaburzyc wspolczynniki d_n i powtorzyc zamiane bazy? (t/n)\n";
    cin >> disruption;
    if (disruption == 't')
    {
        double d1[degree + 1];
        disruptCoefficients(degree, d, d1);
        Polynomial f1_n = d1[0] * Q[0];
        printData(degree, d1, q, Q, f1_n);
        double e1[degree + 1];
        changePolynomialBase(degree, d1, e1, A, B, C, alpha, beta, gamma);
        printResult(degree, e1, q, f1_n);
        cout << "\nZaburzenie danego wielomianu:\n";
        for (int i = degree; i > 0; i--)
            cout << d[i] - d1[i] << "*(" << Q[i] << ") + ";
        cout << d[0] - d1[0];
        cout << " = \n" << f_n - f1_n;
        cout << "\n\nZaburzenie wyniku\n";
        for (int i = degree; i > 0; i--)
            cout << e[i] - e1[i] << "*(" << q[i] << ") + ";
        cout << e[0] - e1[0];
        Polynomial L1_n = (e[0] - e1[0]) * q[0];
        for (int i = 1; i <= degree; i++)
            L1_n += (e[i] - e1[i]) * q[i];
        cout << " = \n" << L1_n << "\n";
        cout << "\nBledy wzgledne zaburzen wspolczynnikow d_k\n";
        for (int i = degree; i > 0; i--)
            cout << (d[i] - d1[i]) / d[i] << "; ";
        cout << "\n\nBledy wzgledne zaburzen wspolczynnikow e_k\n";
        for (int i = degree; i > 0; i--)
            cout << (e[i] - e1[i]) / e[i] << "; ";
    }
}

///// wielokrotne powtarzanie zmiany bazy /////

void multipleBaseChange(int degree, double *d, double *A, double *B, double *C,
                        double *alpha, double *beta, double *gamma, Polynomial *Q, Polynomial *q)
{
    int baseChangeRepetitionNumber;
    cout <<
    "\n\nCzy wykonac zamiane baz z lin(Q_k)->lin(q_k)->lin(Q_k) n-krotnie? Jesli tak, podaj wartosc n, wpp. wpisz 0\n";
    cin >> baseChangeRepetitionNumber;
    if (baseChangeRepetitionNumber > 0)
    {
        double d1[degree + 1], e1[degree + 1];
        changePolynomialBase(degree, d, e1, A, B, C, alpha, beta, gamma);
        for (int i = 0; i < baseChangeRepetitionNumber; i++)
        {
            changePolynomialBase(degree, e1, d1, alpha, beta, gamma, A, B, C);
            changePolynomialBase(degree, d1, e1, A, B, C, alpha, beta, gamma);
        }
        changePolynomialBase(degree, e1, d1, alpha, beta, gamma, A, B, C);
        cout << "Po " << baseChangeRepetitionNumber << " przyblizeniach wielomian f ma postac\n";
        for (int i = degree; i > 0; i--)
            cout << d1[i] << "*(" << Q[i] << ") + ";
        cout << d1[0] << " * (" << Q[0] << ") = ";
        Polynomial L1_n = d1[0] * Q[0];
        for (int i = 1; i <= degree; i++)
            L1_n += d1[i] * Q[i];
        cout << L1_n << "\n";
        cout << "Poczatkowy wielomian ma postac\n";
        for (int i = degree; i > 0; i--)
            cout << d[i] << "*(" << Q[i] << ") + ";
        cout << d[0] << " * (" << Q[0] << ") = ";
        Polynomial L_n = d[0] * Q[0];
        for (int i = 1; i <= degree; i++)
            L_n += d[i] * Q[i];
        cout << L_n << "\n";
        cout << "Blad " << baseChangeRepetitionNumber << " przyblizenia wynosi\n";
        for (int i = degree; i > 0; i--)
            cout << d[i] - d1[i] << "*(" << Q[i] << ") + ";
        cout << d[0] - d1[0] << " * (" << Q[0] << ") = ";
        Polynomial E_n = (d[0] - d1[0]) * Q[0];
        for (int i = 1; i <= degree; i++)
            E_n += (d[i] - d1[i]) * Q[i];
        cout << E_n << "\n";
        cout << "Blad " << baseChangeRepetitionNumber << " przyblizenia Q->q wynosi\n";
        Polynomial L2_n = e1[0] * q[0];
        for (int i = 1; i <= degree; i++)
            L2_n += e1[i] * q[i];
        cout << L_n - L2_n << "\n";
    }
}


int main()
{
    srand(time(NULL));
    cout << "Wielomian f_n, bedacy kombinacja liniowa wielomianow Q_n,...,Q_0 z wspolczynnikami d_n,...,d_0 "
                    "zostanie przedstawiony jako kombinacja liniowa wielomianow q_n,...,q_0\n";
    cout << "Podaj stopien wielomianu f_n:\n";
    int degree;
    char random;
    cin >> degree;
    Polynomial q[degree + 1];
    Polynomial Q[degree + 1];


    double alpha[degree + 1], beta[degree + 1], gamma[degree + 1];
    gamma[0] = 0.0;
    selectPolynomialsType(degree, alpha, beta, gamma, "alpha", "beta", "gamma", "Q");


    double d[degree + 1];
    cout << "Losowac d_k? (t/n)\n";
    cin >> random;
    if (random == 'n')
    {
        cout << "Podaj " << degree + 1 << " wspolczynnikow d_k (k=n,...,0)\n";
        for (int i = degree; i >= 0; i--)
            cin >> d[i];
    }
    if (random == 't')
    {
        for (int i = degree; i >= 0; i--)
            d[i] = (double) rand() / (RAND_MAX) * 10;
    }


    double A[degree + 2], B[degree + 2], C[degree + 2];
    C[0] = 0.0;
    selectPolynomialsType(degree, A, B, C, "A", "B", "C", "q");


    orthogonalPolynomials(degree, q, A, B, C);
    orthogonalPolynomials(degree, Q, alpha, beta, gamma);

    Polynomial f_n = d[0] * Q[0];
    printData(degree, d, q, Q, f_n);


    double e[degree + 1];
    changePolynomialBase(degree, d, e, A, B, C, alpha, beta, gamma);

    printResult(degree, e, q, f_n);

    disrupt(degree, d, A, B, C, alpha, beta, gamma, e, Q, q, f_n);

    multipleBaseChange(degree, d, A, B, C, alpha, beta, gamma, Q, q);

    return 0;
}