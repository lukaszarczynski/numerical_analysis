\documentclass[11pt,wide]{mwart}

\usepackage[OT4,plmath]{polski}
\usepackage[utf8]{inputenc}
\usepackage[polish]{babel}
\usepackage{amsmath, amsfonts}
\usepackage{graphicx}
\usepackage{epstopdf}

\title{{\LARGE\textbf{Pracownia z analizy numerycznej}}\\
       {\Large Sprawozdanie z zadania \textbf{P2.7.}}\\
       {\large Prowadzący: dr hab. Paweł Woźny}}
\author{Łukasz Arczyński\thanks{\textit{Nr indeksu}: \texttt{258424}}}
\date{Wrocław, \today\ r.}

\let\oldsqrt\sqrt
\def\sqrt{\mathpalette\DHLhksqrt}
\def\DHLhksqrt#1#2{%
\setbox0=\hbox{$#1\oldsqrt{#2\,}$}\dimen0=\ht0
\advance\dimen0-0.2\ht0
\setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
{\box0\lower0.4pt\box2}}

\begin{document}
\maketitle

\setcounter{equation}{0}
\section{Wstęp}
Wielomiany to jedne z najważniejszych funkcji w analizie numerycznej. Znajdują zastosowanie w prawie wszystkich jej dziedzinach - dowolną funkcję można interpolować wielomianem, lub złożoną z nich funkcją sklejaną. Możliwa jest również aproksymacja funkcji wielomianem, obliczanie całek lub pochodnych korzystając z pewnych wielomianów. Wielomiany są też szeroko używane w innych dziedzinach matematyki, jak i w fizyce i naukach technicznych.

Czasami jednak używanie wielomianów zapisanych w standardowej bazie $lim(x^k)$ może być znacznym utrudnieniem. Przy obliczaniu całek często korzysta się z wielomianów w postaci Jacobiego z specjalnie wybranymi wartościami $\alpha$ i $\beta$, przy interpolacji Lagrange'a należy rozwiązać układ równań liniowych, którego trudność, jak i dokładność przedstawienia rozwiązania w arytmetyce zmiennopozycyjnej zależy od wyboru bazy wielomianowej. W fizyce używa się wielomianów w bazie Laguerre'a do opisu tzw. kwantowego oscylatora harmonicznego - układu fizycznego mającego zastosowanie w wielu dziedzinach fizyki kwantowej.

W niniejszym sprawozdaniu przedstawię działanie metody zamiany baz wielomianów, opisanej w artykule [1]. Sprawdzę dokładność przedstawienia wielomianu w innej bazie, uwarunkowanie zadania zamiany bazy jak i stabilność numeryczną wykonującego je algorytmu.

Testy zostały przeprowadzone za pomocą programu napisanego w języku C++, używając podwójnej precyzji obliczeń (double). Wykresy zostały narysowane za pomocą programu Graph.

\section{Przedstawianie kombinacji liniowej wielomianów w różnych bazach}
\noindent Mając wielomian dany wzorem:
\begin{equation}
f_n(x) = \sum_{k=0}^{n}d_kq_k(x)
\end{equation}
gdzie $q_k$ jest wielomianem $k$-tego stopnia, spełniającym
\begin{align}
&q_{k+1}(x)=(\alpha_kx+\beta_k)q_k(x)-\gamma_kq_{k-1}(x) &q_0(x)=1,
\end{align}
można przedstawić go w postaci
\begin{equation}
f_n(x) = \sum_{k=0}^{n}e_kQ_k(x)
\end{equation}
gdzie $Q_k$ jest wielomianem $k$-tego stopnia, spełniającym
\begin{align}
&Q_{k+1}(x)=(A_kx+B_k)Q_k(x)-C_kQ_{k-1}(x) &Q_0(x)=1.
\end{align}
Aby obliczyć wartości $e_k$, zdefiniujmy $g_{sk}$, takie, że
\begin{equation}
q_k(x) = \sum_{s=0}^{k}g_{sk}Q_s(x)
\end{equation}
Zastępując wartości $q_k(x)$ z wzoru (1) sumą z wzoru (5) otrzymujemy
\begin{equation}
f_n(x) = \sum_{k=0}^{n}d_k\sum_{s=0}^{k}g_{sk}Q_s(x)
\end{equation}
Porównując wzór (6) i (3), można zauważyć, że 
\begin{equation}
e_k = \sum_{s=k}^{n}d_sg_{ks}
\end{equation}
Po pewnych przekształceniach (opisanych dokładnie w [1]) możemy znaleźć rekurencyjny sposób obliczania $g_{kn}$ 
\begin{align}
g_{kn} &= \frac{\alpha_{n-1}}{A_{k-1}}g_{k-1,n-1}-\left(\frac{\alpha_{n-1}B_k}{A_k}-\beta_{n-1}\right)g_{k,n-1}+\frac{\alpha_{n-1}C_{k+1}}{A_{k+1}}g_{k+1,n-1}-\gamma_{n-1}g_{k,n-2} \\ \notag
g_{kn} &= 0 \text{, dla }k<0\text{ lub }k<n
\end{align}

\section{Analizowane klasyczne układy wielomianów ortogonalnych}
\subsection{Wielomiany Legendre'a}
Wielomiany zadane wzorem rekurencyjnym
\begin{align}
P_0(x)&=1 & P_1(x)=x \notag \\
 P_{n+1}(x)&=\frac{2n+1}{n+1}x P_n(x)-\frac{n}{n+1} P_{n-1}(x)
\end{align}
tworzą układ wielomianów ortogonalnych z funkcją wagową $p(x)=1$.
\subsection{Wielomiany Czebyszewa 1. i 2. rodzaju}
Wielomiany Czebyszewa 1. rodzaju zadane są wzorem rekurencyjnym
\begin{align}
T_0(x)&=1 &T_1(x)=x \notag \\ 
T_{n+1}(x)&=2x T_{k}(x) - T_{k-1}(x)
\end{align}
i tworzą układ ortogonalny z funkcją wagową $\displaystyle \frac{1}{\sqrt{1-x^2}}$. \\
Wielomiany Czebyszewa 2. rodzaju $U_n$ są zadane takim samym równaniem rekurencyjneym, ale\\ $U_1=2x$, tworzą układ wielomianów ortogonalnych z wagą $\sqrt{1-x^2}$.
\subsection{Wielomiany Hermite'a}
\begin{align}
H_0(x)&=1 & H_1(x)=2x \notag \\ 
H_{n+1}(x)&=2xH_{n}(x) - 2nH_{n-1}(x) 
\end{align}
Tworzą układ wielomianów ortogonalnych z funkcją wagową $e^{-x^2}$.
\subsection{Wielomiany Laguerre'a}
W publikacji [3] można znaleźć wzór rekurencyjny (przyjmując $L_n=L_n^{(0)}$ i $\alpha = 0$):
$$
(n+1)L_{n+1}(x) = (2n+1-x)L_n(x)-nL_{n-1}(x)
$$
Czyli
\begin{align}
&L_{n+1}(x) = (\frac{2n+1}{n+1}-\frac{x}{n+1})L_n(x)-\frac{n}{n+1}L_{n-1}(x) \\ \notag
\text{Ponadto} \\ \notag
&L_0(x) = 1 &L_1(x) = 1-x
\end{align}
Wielomiany te tworzą układ ortogonalny z funkcją wagową $e^{-x}$.
\subsection{Wielomiany Gegenbauera}
Wielomiany Gegenbauera są uogólnieniem wielomianów Czebyszewa i Legendre'a, są zadane wzorem rekurencyjnym
$$
C_0^\lambda(x)  = 1;  C_1^\lambda(x)  = 2 \lambda x
$$
\begin{equation}
C_{n+1}^\lambda(x)  = \frac{2(n+\lambda)}{n+1}xC_n^\lambda(x) - \frac{n+2\lambda-1}{n+1}C_{n-1}^\lambda(x)
\end{equation}
Są ortogonalne z wagą $(1-x^2)^{\lambda-\frac{1}{2}}$
\subsection{Wielomiany Jacobiego}
Wielomiany Jacobiego są uogólnieniem wielomianów Gegenbauera, i tworzą układ wielomianów ortogonalnych z funkcją wagową $(1-x)^\alpha(1+x)^\beta$.\\
W publikacji [2] można znaleźć wzór rekurencyjny:
\begin{align*}
&2n (n + \alpha + \beta) (2n + \alpha + \beta - 2) P_n^{(\alpha,\beta)}(x) = \\
(&2n+\alpha + \beta-1) \left( (2n+\alpha + \beta)(2n+\alpha+\beta-2) x +  \alpha^2 - \beta^2 \right) P_{n-1}^{(\alpha,\beta)}(x) \\
- &2 (n+\alpha - 1) (n + \beta-1) (2n+\alpha + \beta) P_{n-2}^{(\alpha, \beta)}(x)
\end{align*}
Po zwiększeniu $n$ o $1$ i przekształceniu otrzymujemy wzór w postaci $q_{k+1}(x)=(\alpha_kx+\beta_k)q_k(x)-\gamma_kq_{k-1}(x)$
\begin{align}
P_{n+1}^{(\alpha,\beta)}(x) = \\ \notag
\left(\frac{(2n+\alpha + \beta+1) (2n+ \alpha + \beta+2)}{(2n+2) (n + \alpha + \beta+ 1 ) } x + \frac{(2n+\alpha + \beta+1)( \alpha^2 - \beta^2)}{(2n+2) (n + \alpha + \beta+ 1 ) (2n + \alpha + \beta) } \right) &P_{n}^{(\alpha,\beta)}(x) \\ \notag
 - \frac{2 (n+\alpha) (n + \beta) (2n+\alpha + \beta +2)}{(2n+2) (n + \alpha + \beta+ 1 ) (2n + \alpha + \beta) } &P_{n-1}^{(\alpha, \beta)}(x)
\end{align}
Ponadto
\begin{align*}
&P_0^{(\alpha,\beta)}(x) = 1
&P_1^{(\alpha,\beta)}(x) = \frac{\alpha+\beta+2}{2}x-\frac{\beta-\alpha}{2}
\end{align*}

\section{Analiza wybranych wyników testów}
Dla prostych testów, takich jak przedstawienie $x^8$ za pomocą wielomianów $T_n$ $(n=0,...,8)$ otrzymujemy wynik
$f_n = 0.0078125*(128x^8 - 256x^6 + 160x^4 - 32x^2 + 1) + 0*(64x^7 - 112x^5 + 56x
^3 - 7x) + 0.0625*(32x^6 - 48x^4 + 18x^2 - 1) + 0*(16x^5 - 20x^3 + 5x) + 0.21875
*(8x^4 - 8x^2 + 1) + 0*(4x^3 - 3x) + 0.4375*(2x^2 - 1) + 0*(x) + 0.2734375 * (1)
= x^8$. Błąd przedstawienia jest równy 0.

Dlatego w kolejnych testach wartości $d_k$ będą losowymi liczbami z przedziału $[0,10]$. W pliku \texttt{wyniki.txt} przedstawione są wyniki zamiany  losowej kombinacji liniowej wielomianów Czebyszewa 1. rodzaju (stopnia nie większego od $8$) na kombinację liniową wielomianów Czebyszewa 2. rodzaju. Błąd przedstawienia jest równy $-2.27374*10^{-13}x^6 - 1.33227*10^{-15}$. Mimo losowania $d_k$, wartości błędów przy większości wyrazów $x^k$ są mniejsze od użytej precyzji arytmetyki, więc są wypisywane jako 0.
Niech 
$$
E(x) := \text{błąd przedstawienia } x
$$
Wzór (15) przedstawia błąd zamiany kombinacji liniowej z losowymi współczynnikami $d_k$ wielomianów Gegenbauera $C_k^{3.1415926535}$ stopnia $\le8$ na kombinację liniową wielomianów $Q_k$ z losowymi wartościami $A_k$, $B_k$ i $C_k$ (losowymi wielomianami ortogonalnymi). Na rysunku 1. przedstawiony jest wykres tego błędu.
\begin{align}
E(f_n(x)) = 2.91*10^{-11}&x^8 + 1.42*10^{-10}x^7 - 6.7*10^{-10}x^6 + 3.3*10^{-9}x^5  \\ \notag
- 1.1*10^{-9}&x^4 + 3*10^{-9}x^3 + 9*10^{-10}x^2 + 2*10^{-10}x + 3.1*10^{-11}
\end{align}

\begin{center}
  \begin{figure}[h]
    \caption{Wykres błędu E(x) z wzoru (15), $x \in [-9.5,6,5]$}
      \begin{center}
	\includegraphics[width=14cm]{gegenbauer.eps}
      \end{center}
  \end{figure}
\end{center}

Funkcja $E(f_n(x))$ z wzoru (15) ma minimum w $-7,73698\dots$. Jest to jedyny punkt, w którym $E'(f_n(x)) = 0$. Funkcje błędów z pozostałych testów nie mają takiej własności, ale większość z nich ma lokalne ekstremum, wyraźnie widoczne na wykresie.

Wzór (16) przedstawia błąd przedstawienia kombinacji liniowej z losowymi współczynnikami $d_k$ wielomianów Legendre'a stopnia $\le20$ za pomocą kombinacji liniowej wielomianów Czebyszewa 2. rodzaju. Na rysunku 2. przedstawiony jest wykres tego błędu.

\begin{align}
E(f_n(x)) = 9.31*10^{-10}x^{15} &+ 9.31*10^{-10}x^{12} - 4.66*10^{-10}x^{11} - 2.33*10^{-10}x^{10}  \\
 + 1.16*10^{-10}x^8 &- 7.28*10^{-12}x^6 - 7.28*10^{-12}x^5  \notag \\ 
+ 1.36*10^{-12}x^4 &+ 4.55*10^{-13}x^3 + 1.42*10^{-14}x^2 + 2.22*10^{-16} \notag
\end{align}

\begin{center}
  \begin{figure}[h]
    \caption{Wykres błędu E(x) z wzoru (16), $x \in [-1.15,1]$}
      \begin{center}
	\includegraphics[width=14cm]{legendre.eps}
      \end{center}
  \end{figure}
\end{center}

Wzór (17) przedstawia błąd zmiany kombinacji liniowej wielomianów Jacobiego $P_n^{(0.37,0.73)}$,\\ $(n\le12)$ na kombinację liniową wielomianów Laguerre'a. Na rysunku 3. przedstawiony jest wykres tego błędu.

\begin{align}
E(f_n(x)) = -9.095*10^{-13}x^12 &+ 9.095*10^{-13}x^11 + 5.457*10^{-12}x^10 + 8.185*10^{-12}x^9 \\
- 1.819*10^{-11}x^8 &- 10^{-11}x^7 + 6.003*10^{-11}x^6 + 5.707*10^{-11}x^5 - 4.434*10^{-11}x^4 \notag \\ \notag
- 9.081*10^{-12}x^3 &+ 3.234*10^{-11}x^2 + 1.961*10^{-12}x - 5.086*10^{-12}
\end{align}

\begin{center}
  \begin{figure}[h]
    \caption{Wykres błędu E(x) z wzoru (17), $x \in [-1,1]$}
      \begin{center}
	\includegraphics[width=14cm]{jacobi.eps}
      \end{center}
  \end{figure}
\end{center}

Funkcje błędu względnego w dla każdego $x$, oprócz okolicy miejsc zerowych $f_n(x)$, dla wszystkich przeprowadzonych testów, spełniają zależność 
\begin{equation}
\frac{E(f_n(x))}{f_n(x)} \approx \varepsilon
\end{equation}
Gdzie $\varepsilon$ jest rzędu precyzji arytmetyki.

Pełne wyniki 3 powyższych testów można znaleźć w plikach odpowiednio \texttt{gegenbauer.txt}, \texttt{legendre.txt} i \texttt{jacobi.txt}

\section{Uwarunkowanie zadania}
Zadanie jest dobrze uwarunkowane, jeśli niewielkie względne zmiany danych skutkują niewielkimi względnymi zmianami wyniku. Aby sprawdzić uwarunkowanie zadania zmiany bazy wielomianu, zaburzyłem każdy współczynnik $d_k$ z wzoru (1) o nie więcej niż $0.1d_k$. Następnie porównałem te zaburzenia z zaburzeniami współczynników $d_k$. 

W Tablicy 1. przedstawione są zaburzenia współczynników $d_k$ i $e_k$ dla zadania zamiany wielomianów Czebyszewa 1. rodzaju na wielomiany Czebyszewa 2. rodzaju, opisanego w rozdziale 4.

\begin{table}[!h]
\renewcommand{\arraystretch}{1.5}
\caption{Błędy wzgledne zaburzeń współczynników $e_k$ i $d_k$}
\begin{center}
\begin{tabular}{|c||c|c|c|c|c|c|c|c|} \hline
k & 8&7&6&5&4&3&2&1 \\ \hline \hline
$d_k$ & -0.02758 & -0.08359 & -0.02238 & -0.0391 & -0.01478 & -0.01728 & -0.08524 & 0.0308 \\ \hline
$e_k$ & -0.02758 & -0.08359 & -0.0809 & -0.11642 & 0.04021 & -0.06156 & -0.23837 & 0.04881 \\ \hline
\end{tabular}
\end{center}
\end{table}
\renewcommand{\arraystretch}{1}

Jak widać, dla $k=7;8$, zaburzenia $d_k=e_k$ są takie same. Gdy zamieniamy kombinację liniową wielomianów ortogonalnych stopnia nie większego od $n$, zaburzenie $d_n$ będzie zawsze równe zaburzeniu $e_n$, ponieważ $x^n$ występuje w tej kombinacji liniowej dokładnie raz. Jeśli  w początkowej i docelowej kombinacji liniowej wyraz $x^k$ występuje dokładnie raz (jak np. $x^{n-1}$ w kombinacji liniowej wielomianów Czebyszewa, bo $T_n=a_nx^n+a_{n-2}x^{n-2}+\ldots$), to zaburzenie $d_k$ będzie równe zaburzeniu $e_k$. 

Niech 
$$
z(x) := \text{zaburzenie x}
$$
Widać również, że zaburzenie $e_k$ nieznacznie wpłynęło na wartości $d_k$. Co więcej, w innym teście, dla $n=100$, pomimo, że zaburzenia $d_k$ są wybrane losowo z przedziału $[0,9d_k;1,1d_k]$, to dla każdego k $|z(e_k)|\approx0,012e_k$ lub $|z(e_k)|\approx0,033$. Dla $n=500$ jedynie 2 wartości bezwzględne zaburzeń $e_k$ są większe od $0,01e_k$ (wartości zaburzeń dla $n=100$ i $n=500$ są przedstawione w plikach odpowiednio \texttt{zaburzenia sto.txt} i \texttt{zaburzenia piecset.txt}). Więc można przypuszczać, że dla $n\to\infty$, $z(e_n)\to\frac{z(d_1)+z(d_2)+\ldots+z(d_n)}{n}$.
\\ \noindent Wniosek: Zadanie jest dobrze uwarunkowane.

\section{Zmiana błędu przedstawienia po wielokrotnej zamianie bazy}
Aby sprawdzić stabilność numeryczną algorytmu, wykonałem m-krotnie zamianę bazy z $lin(q_k)$ w $lin(Q_k)$, następnie z $lin(Q_k)$ w $lin(q_k)$.
W przypadku zadania zamiany wielomianów Czebyszewa 1. rodzaju (stopnia nie większego niż $8$) na wielomiany Czebyszewa 2. rodzaju (opisanego w rozdziale 4.) błąd przedstawienia $q_k$ dla $m=50$ jest równy $0$. Następne testy zostaną przeprowadzone dla większego $m$ lub większego $n$ (stopień wielomianu $f_n$ z wzoru (1)).

Błąd przedstawienia dla zamiany wielomianów Gegenbauera $C_k^{3.1415926535}$ stopnia $\le8$ na losowe wielomiany ma wartość opisaną wzorem (15). Błąd setnego przedstawienia tych samych wielomianów jest opisany wzorem
\begin{align*}
2.02*10^{-9}&x^8 + 1.27*10^{-8}x^7 + 7.94*10^{-8}x^6 + 2.23*10^{-7}x^5 \\
 - 6.02*10^{-8}&x^4 - 2.91*10^{-7}x^3 - 1.26*10^{-7}x^2 + 8.41*10^{-8}x - 4.24*10^{-8}
\end{align*}
Jak widać, błąd setnego przedstawienia jest w przybliżeniu stukrotnie większy od błędu pierwszego przedstawienia.
Sprawdźmy, czy podobna zależność występuje w innych przypadkach.
Po zamianie wielomianów Legendre'a (stopnia $\le 20$) na wielomiany Czebyszewa 2. rodzaju, błąd przybliżenia wyraża się wzorem (16), natomiast po 100 000 zamianach błąd jest równy
\begin{align*}
&-4.657*10^{-10}x^{18} - 2.578*10^{-6}x^{17} - 8.14*10^{-7}x^{16} + 1.017*10^{-5}x^{15} + 2.718*10^{-6}x^{14} \\
&- 1.664*10^{-5}x^{13} - 3.645*10^{-6}x^{12} + 1.457*10^{-5}x^{11} + 2.527*10^{-6}x^{10} - 7.349*10^{-6}x^9 \\
&- 9.805*10^{-7}x^8 + 2.138*10^{-6}x^7 + 2.222*10^{-7}x^6 - 3.36*10^{-7}x^5 - 3.018*10^{-8}x^4 \\
&+ 2.352*10^{-8}x^3 + 2.083*10^{-9}x^2 - 3.004*10^{-10}x - 2.513*10^{-11}
\end{align*}
Po porównianiu powyższego wzoru z wzorem (16), widzimy, że błąd stutysięcznegoprzedstawienia jest w przybliżeniu 100 000 razy większy od błędu pierwszego przedstawienia. W powyższym wzorze występują również błędy przy takich wartościach $x^k$, które we wzorze (16) były mniejsze od precyzji arytmetyki (część z nich mogła być tak mała z powodu wyboru $d_k$, $q_k$ lub $Q_k$). 

Dokładne wyniki 2 powyższych testów można znaleźć w plikach odpowiednio \texttt{stokrotnie.txt} i \texttt{sto tysiecy.txt}.

Można wnioskować, że 
\begin{equation}
\text{błąd przybliżenia } e_k \sim m.
\end{equation}
Powyższą tezę potwierdzają również wyniki testu dla $n=100$ i $m=10^7$, jak i wyniki innych testów.

Wniosek: Algorytm jest numerycznie stabilny. \\

Zadanie jest dobrze uwarunkowane, algorytm jest numerycznie stabilny, a błędy względne są rzędu precyzji używanej arytmetyki. Jak widać, przedstawiona metoda bardzo dobrze nadaje się do zamiany baz wielomianów ortogonalnych.

\newpage
\thispagestyle{empty}
\begin{thebibliography}{99}
 \bibitem{} B.Y. Ting, Y. Luke, \textit{Conversion of Polynomials between Different Polynomial Bases},\\ IMA Journal of Numerical Analysis 1 (1981), 229–234
 \bibitem{} Haiyun Yu, \textit{Orthogonal Polynomials and Related Approximation Results}, http://lsec.cc.ac.cn/\~{}hyu/teaching/shonm2013/STWchap3.2p.pdf
 \bibitem{} Ole Warnaar, \textit{(q;t)-Laguerre polynomials},\\ http://www.maths.uq.edu.au/MASCOS/Orthogonal09/Warnaar.pdf
\end{thebibliography}
\end{document}

