// Łukasz Arczyński
// nr. indeksu 258424
// zad P2.7
// Wrocław, 14 XII 2013 r.

#ifndef POLYNOMIAL
#define POLYNOMIAL

#include <iostream>

using namespace std;

class Polynomial
{
private:
    int degree;
    double *coefficients;
    friend istream &operator >>(istream &we, Polynomial &);
    friend ostream &operator <<(ostream &wy, const Polynomial &);
    friend Polynomial operator +(const Polynomial &, const Polynomial &);
    friend Polynomial operator -(const Polynomial &, const Polynomial &);
    friend Polynomial operator *(const Polynomial &, const double &q);
    friend Polynomial operator *(const double &q, const Polynomial &);
    friend Polynomial operator *(const Polynomial &, const Polynomial &);

public:
    Polynomial();
    Polynomial(int st, double wsp = 1.0);
    Polynomial(int st, double wsp[]);
    Polynomial(const Polynomial &w);
    ~Polynomial();

    Polynomial &operator +=(const Polynomial &w);
    Polynomial &operator -=(const Polynomial &w);
    Polynomial &operator *=(const Polynomial &w);

    Polynomial &operator =(const Polynomial &w)
    {
        if (this != &w)
        {
            delete[] coefficients;
            degree = w.degree;
            coefficients = new double[degree];
            for (int i = 0; i < degree; i++)
                coefficients[i] = w.coefficients[i];
        }
        return *this;
    }
};

#endif // POLYNOMIAL
