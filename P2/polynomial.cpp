// Łukasz Arczyński
// nr. indeksu 258424
// zad P2.7
// Wrocław, 14 XII 2013 r.

#include <iostream>
#include <cmath>
#include "polynomial.h"

using namespace std;

Polynomial::Polynomial()
{
    degree = 0;
    coefficients = new double[1];
    coefficients[0] = 0;
}

Polynomial::Polynomial(int s, double _coefficients[])
{
    if (s < 0)
        throw string("Blad: Ujemny stopien");
    degree = s + 1;
    coefficients = new double[degree];
    for (int i = 0; i < degree; i++)
        coefficients[i] = _coefficients[i];
}

Polynomial::Polynomial(const Polynomial &w)
{
    degree = w.degree;
    coefficients = new double[degree];
    for (int i = 0; i < degree; i++)
        coefficients[i] = w.coefficients[i];
}

Polynomial::~Polynomial()
{
    delete[] coefficients;
    degree = 0;
}

///// Działania arytmetyczne /////

Polynomial operator +(const Polynomial &w, const Polynomial &u)
{
    int new_degree = w.degree > u.degree ? w.degree : u.degree;
    double new_coefficients[new_degree];
    for (int i = 0; i < new_degree; i++)
        new_coefficients[i] = 0;
    for (int i = 0; i < w.degree; i++)
        new_coefficients[i] += w.coefficients[i];
    for (int i = 0; i < u.degree; i++)
        new_coefficients[i] += u.coefficients[i];
    for (int i = new_degree - 1; i > 0 && new_coefficients[i] == 0; i--)
        new_degree--;
    return Polynomial(new_degree - 1, new_coefficients);
}

Polynomial operator -(const Polynomial &w, const Polynomial &u)
{
    int new_degree = w.degree > u.degree ? w.degree : u.degree;
    double new_coefficients[new_degree];
    for (int i = 0; i < new_degree; i++)
        new_coefficients[i] = 0;
    for (int i = 0; i < w.degree; i++)
        new_coefficients[i] += w.coefficients[i];
    for (int i = 0; i < u.degree; i++)
        new_coefficients[i] -= u.coefficients[i];
    for (int i = new_degree - 1; i > 0 && new_coefficients[i] == 0; i--)
        new_degree--;
    return Polynomial(new_degree - 1, new_coefficients);
}

Polynomial operator *(const Polynomial &w, const double &f)
{
    int new_degree = w.degree;
    double new_coefficients[new_degree];
    for (int i = 0; i < w.degree; i++)
        new_coefficients[i] = w.coefficients[i] * f;
    return Polynomial(new_degree - 1, new_coefficients);
}

Polynomial operator *(const double &f, const Polynomial &w)
{
    return w * f;
}

Polynomial operator *(const Polynomial &w, const Polynomial &u)
{
    if (u.degree == 1)
        return w * u.coefficients[0];
    if (w.degree == 1)
        return u * w.coefficients[0];
    if (w.degree > u.degree)
        return u * w;
    int new_degree = w.degree + u.degree - 1;
    double new_coefficients[new_degree];
    for (int i = 0; i < new_degree; i++)
        new_coefficients[i] = 0;
    for (int i = 0; i < w.degree; i++)
        for (int j = 0; j < u.degree; j++)
            new_coefficients[i + j] += w.coefficients[i] * u.coefficients[j];
    for (int i = new_degree - 1; i > 0 && new_coefficients[i] == 0; i--)
        new_degree--;
    return Polynomial(new_degree - 1, new_coefficients);
}

Polynomial &Polynomial::operator +=(const Polynomial &w)
{
    *this = *this + w;
    return *this;
}

Polynomial &Polynomial::operator -=(const Polynomial &w)
{
    *this = *this - w;
    return *this;
}

Polynomial &Polynomial::operator *=(const Polynomial &w)
{
    *this = *this * w;
    return *this;
}

///// Wczytywanie i wypisywanie wielomianów /////

istream &operator >>(istream &input, Polynomial &w)
{
    input >> w.degree;
    w.degree++;
    delete[] w.coefficients;
    w.coefficients = new double[w.degree];
    for (int i = w.degree - 1; i >= 0; i--)
        input >> w.coefficients[i];
    return input;
}

ostream &operator <<(ostream &output, const Polynomial &w)
{
    for (int i = w.degree - 1; i > 0; i--)
        if (w.coefficients[i] != 0)
        {
            if (w.degree - 1 != i)
            {
                if (w.coefficients[i] > 0)
                    output << " + ";
                else
                    output << " - ";
            }
            else if (w.coefficients[i] < 0)
                output << "-";
            if (fabs(w.coefficients[i]) != 1.0)
                output << fabs(w.coefficients[i]);// << "*";
            if (i == 1)
                output << "x";
            else
                output << "x^" << i;
        }
    if (w.coefficients[0] != 0)
    {
        if (w.coefficients[0] > 0 && w.degree - 1 != 0)
            output << " + ";
        if (w.coefficients[0] < 0)
            output << " - ";
        output << fabs(w.coefficients[0]);
    }
    return output;
}