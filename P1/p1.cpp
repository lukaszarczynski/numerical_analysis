// Łukasz Arczyński
// nr. indeksu 258424
// zad P1.10
// Wrocław, 11 XI 2013 r.

#include <iostream>
#include <cmath>
#include <iomanip>
#include <fstream>
#include "p1.h"

using namespace std;

long double Tab[10][20];

//////////////////////// OBLICZANIE 20 PIERWSZYCH WYRAZOW CIAGOW ////////////////////////

long double aSeries()
{
    Tab[0][0] = 1.0L;
    for (int i = 1; i < 20; i++)
        Tab[0][i] = Tab[0][i - 1] + pow(-1L, i) * pow(2L * i + 1L, -1L);
    return PI_4;
}

long double bSeries()
{
    Tab[0][0] = 1.0L;
    for (int i = 1; i < 20; i++)
        Tab[0][i] = Tab[0][i - 1] + pow(i + 1, -1.5);
    return SUM_OF_B_SERIES;
}

long double cSeries()
{
    for (int i = 0; i < 20; i++)
        Tab[0][i] = pow(1 + (1 / (long double) (i + 1)), i + 1);
    return E;
}

long double dSeries()
{
    long double factorial[20];
    factorial[0] = 1L;
    for (int i = 1; i < 20L; i++)
        factorial[i] = i * factorial[i - 1];
    Tab[0][0] = 1;
    for (int i = 1; i < 20; i++)
        Tab[0][i] = Tab[0][i - 1] + (1L / factorial[i]);
    return E;
}

long double fibonacciNumbers()
{
    long double fibonacci[21];
    fibonacci[0] = 1L;
    fibonacci[1] = 1L;
    for (int i = 2; i < 21; i++)
        fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
    for (int i = 0; i < 20; i++)
        Tab[0][i] = fibonacci[i + 1] / fibonacci[i];
    return FI;
}

long double wallisNumbers()
{
    Tab[0][0] = 8.0L / 3.0L;
    for (int i = 2; i < 21; i++) //inna implementacja, bez uzycia (!!)
        Tab[0][i - 1] = (Tab[0][i - 2] * 4L * i * i) / ((2L * i - 1L) * (2 * i + 1L));
    return PI;
}

long double stirlingNumbers()
{
    long double factorial[21];
    factorial[0] = 1L;
    for (int i = 1L; i < 21L; i++)
        factorial[i] = i * factorial[i - 1];
    for (int i = 1; i < 21; i++)
        Tab[0][i - 1] = (factorial[i] * powl(E, i)) / (sqrtl(2.0L * PI * i) * powl(i, i));
    return 1.0L;
}

//////////////////////// OBLICZANIE MIEJSC ZEROWYCH ////////////////////////

long double F(long double x)
{
    return x * x - cosl(2L * x) * cosl(2L * x);
}

long double f(long double x)
{
    return 2L * (sinl(4L * x) + x);
}

void newtonsMethod()
{
    Tab[0][0] = 0.73L;
    for (int i = 1; i < 20; i++)
        Tab[0][i] = Tab[0][i - 1] - (F(Tab[0][i - 1]) / f(Tab[0][i - 1]));
}

void simplifiedNewtonsMethod()
{
    Tab[0][0] = 0.73L;
    for (int i = 1; i < 20; i++)
        Tab[0][i] = Tab[0][i - 1] - (F(Tab[0][i - 1]) / (DERRIVATIVE_OF_F_0));
}

void secantMethod()
{
    Tab[0][0] = 0.73L;
    Tab[0][1] = 0.37L;
    for (int i = 2; i < 20; i++)
        Tab[0][i] = Tab[0][i - 1] -
                    F(Tab[0][i - 1]) * (Tab[0][i - 1] - Tab[0][i - 2]) / (F(Tab[0][i - 1]) - F(Tab[0][i - 2]));
}

long double banachFixedPointIteration()
{
    Tab[0][0] = 1.8;
    for (int i = 1; i < 20; i++)
        Tab[0][i] = sqrtl(Tab[0][i - 1] + 10) /
                    Tab[0][i - 1]; // funkcja p(x)=x^4-x-10 przeksztalcona do postaci x=sqrt(x+10)/x
    return ZERO_OF_POLYNOMIAL;
}

long double steffensensMethod()
{
    Tab[0][0] = 1.8;
    for (int i = 1; i < 20; i++)
    {
        if (i % 3 == 0) // obliczanie 1 wartosci met. Aitkena
            Tab[0][i] = Tab[0][i - 3] - ((Tab[0][i - 2] - Tab[0][i - 3]) * (Tab[0][i - 2] - Tab[0][i - 3]) /
                                         (Tab[0][i - 1] - 2 * (Tab[0][i - 2]) + Tab[0][i - 3]));
        else // obliczanie 2 kolejnych wartosci met. iteracji prostej Banacha
            Tab[0][i] = sqrtl(Tab[0][i - 1] + 10) / Tab[0][i - 1];
    }
    return ZERO_OF_POLYNOMIAL;
}

//////////////////////// METODA AITKENA ////////////////////////

void aitkensDeltaSquared()
{
    for (int j = 1; j < 10; j++)
        for (int i = 0; i < 20 - 2 * j; i++)
            Tab[j][i] = (Tab[j - 1][i] * Tab[j - 1][i + 2] - Tab[j - 1][i + 1] * Tab[j - 1][i + 1]) /
                        (Tab[j - 1][i + 2] - 2L * Tab[j - 1][i + 1] + Tab[j - 1][i]);
}

void improvedAitkensMethod()
{
    for (int j = 1; j < 10; j++)
        for (int i = 0; i < 20 - 2 * j; i++)
            Tab[j][i] = Tab[j - 1][i] - ((Tab[j - 1][i + 1] - Tab[j - 1][i]) * (Tab[j - 1][i + 1] - Tab[j - 1][i]) /
                                         (Tab[j - 1][i + 2] - 2 * Tab[j - 1][i + 1] + Tab[j - 1][i]));
}

//////////////////////// WYPISYWANIE WYNIKOW ////////////////////////

void printResults(int precision, long double result)
{
    if (precision > 5) // przy wypisywaniu wiecej niz 5 cyfr po przecinku najpierw wypisuje obliczone wartosci ciagow, bledy, logarytmy z bledow
    {                  // potem kolejne wyniki uzycia metody Aitkena
        for (int j = 0; j < 10; j++)
        {
            for (int i = 0; i < 20 - 2 * j; i++)
                cout << fixed << setprecision(precision) << Tab[j][i] << "  err=" << abs(Tab[j][i] - result)
                << "  -log10(err)=" << setprecision(4) << -log10(abs(Tab[j][i] - result)) << "\n";
            cout << "\n\n";
        }
    }
    else // dla precyzji mniejszej od 6 wypisywanie w postaci 3 tablic trojkatnych
    {
        for (int i = 0; i < 20; i++) // tablica wynikow
        {
            for (int j = 0; j < 10 - i / 2; j++)
                cout << setw(precision + 2) << fixed << setprecision(precision) << Tab[j][i] << " ";
            if (precision != 5 || i >= 2) cout << "\n";
        }
        cout << "\nBledy\n";
        for (int i = 0; i < 20; i++) //tablica bledow
        {
            for (int j = 0; j < 10 - i / 2; j++)
                cout << setw(precision + 2) << fixed << setprecision(precision) << abs(Tab[j][i] - result) << " ";
            if (precision != 5 || i >= 2) cout << "\n";
        }
        cout << "\nDokladnosc (-log_10(err))";
        for (int i = 0; i < 20; i++) // tablica logarytmow z bledow
        {
            for (int j = 0; j < 10 - i / 2; j++)
                cout << setw(precision + 2) << fixed << setprecision(precision - 1) << -log10(abs(Tab[j][i] - result)) << " ";
            if (precision != 5 || i >= 2) cout << "\n";
        }
    }
    cout << "\n\n";
}

//////////////////////// ZAPISYWANIE NIEKTORYCH WYNIKOW DO PLIKOW ////////////////////////

void bSeriesToFile(const char *filename, long double result)
{
    long double Tab100[50][100];
    Tab100[0][0] = 1.0L;
    for (int i = 1; i < 100; i++) // obliczanie 100 wartosci ciagu
        Tab100[0][i] = Tab100[0][i - 1] + pow(i + 1, -1.5);

    aitkenDeltaSquared50Times(Tab100); // metoda Aitkena (numerycznie bardziej poprawna)
    saveFile(filename, Tab100, result);
}

void cSeriesToFile(const char *filename, long double result)
{
    long double Tab100[50][100];
    for (int i = 0; i < 100; i++) // obliczanie 100 wartosci ciagu
        Tab100[0][i] = pow(1 + (1 / (long double) (i + 1)), i + 1);

    aitkenDeltaSquared50Times(Tab100); // metoda Aitkena (numerycznie bardziej poprawna)
    saveFile(filename, Tab100, result);
}

void wallisNumbersToFile(const char *filename, long double result)
{
    long double Tab100[50][100];
    Tab100[0][0] = 8.0L / 3.0L;
    for (int i = 2; i < 101; i++) // obliczanie 100 wartosci ciagu
        Tab100[0][i - 1] = (Tab100[0][i - 2] * 4L * i * i) / ((2L * i - 1L) * (2 * i + 1L));

    aitkenDeltaSquared50Times(Tab100); // metoda Aitkena (numerycznie bardziej poprawna)
    saveFile(filename, Tab100, result);
}

void aitkenDeltaSquared50Times(long double Tab100[50][100])
{
    for (int j = 1; j < 50; j++) // metoda Aitkena (numerycznie bardziej poprawna)
        for (int i = 0; i < 100 - 2 * j; i++)
            Tab100[j][i] = Tab100[j - 1][i] -
                           ((Tab100[j - 1][i + 1] - Tab100[j - 1][i]) * (Tab100[j - 1][i + 1] - Tab100[j - 1][i]) /
                            (Tab100[j - 1][i + 2] - 2 * Tab100[j - 1][i + 1] + Tab100[j - 1][i]));
}

void saveFile(const char *filename, long double Tab100[50][100], long double result)
{
    fstream file(filename, ios::out);
    for (int i = 0; i < 100; i++) // wypisywanie do pliku
    {
        for (int j = 0; j < 50 - i / 2; j++)
            file << setw(5) << fixed << setprecision(2) << -log10(abs(Tab100[j][i] - result)) << " ";
        file << "\n";
    }
}

//////////////////////// MAIN ////////////////////////

int main()
{
    int nr, precision, mode;
    long double result = 0.0L;
    for (; ;)
    {
        cout << "Wartosci ktorego ciagu obliczyc\n0.  Zakoncz\n1.  s(n)=suma(((-1)^j)(2j+1)^-1) od j=0 do n\n"
                "2.  s(n)=suma(k^(-3/2)) od k=1 do n\n3.  s(n)=(1+(1/n))^n\n4.  s(n)=suma(1/i!) od i=0 do n\n"
                "5.  s(n)=F(n+1)/F(n)\n6.  s(n)=((2n(!!))^2)/(2n-1)(!!)(2n+1)(!!) (wzor Wallisa)\n"
                "7.  s(n)=n!e^n/((n^n)(sqrt(2*pi*n)) (wzor Stirlinga)\n8.  Znajdz zero funkcji f(x)=x^2-cos^2(2x)\n"
                "9.  Znajdz zero funkcji f(x)=x^4-x-10 metoda iteracji prostej Banacha\n10. Znajdz zero funkcji f(x)=x^4-x-10 metoda Steffensena\n"
                "12. Oblicz 100 pierwszych wyrazow ciagu 2., zastosuj metode Aitkena 50 razy,\n    wyniki zapisz do pliku 102.txt\n"
                "13. Oblicz 100 pierwszych wyrazow ciagu 3., zastosuj metode Aitkena 50 razy,\n    wyniki zapisz do pliku 103.txt\n"
                "16. Oblicz 100 pierwszych wyrazow ciagu 6., zastosuj metode Aitkena 50 razy,\n    wyniki zapisz do pliku 106.txt\n";
        cin >> nr;
        cout << "\n";
        switch (nr)
        {
            case 0:
                return 0;
            case 1:
                result = aSeries();
                break;
            case 2:
                result = bSeries();
                break;
            case 3:
                result = cSeries();
                break;
            case 4:
                result = dSeries();
                break;
            case 5:
                result = fibonacciNumbers();
                break;
            case 6:
                result = wallisNumbers();
                break;
            case 7:
                result = stirlingNumbers();
                break;
            case 8:
                result = ZERO_OF_F;
                break;
            case 9:
                result = banachFixedPointIteration();
                break;
            case 10:
                result = steffensensMethod();
                break;
            case 12:
                bSeriesToFile("102.txt", SUM_OF_B_SERIES);
                break;
            case 13:
                cSeriesToFile("103.txt", E);
                break;
            case 16:
                wallisNumbersToFile("106.txt", PI);
                break;
            default:
                return 0;
        }
        if (nr == 8)
        {
            int method;
            cout << "Uzyc: \n(1) metody Newtona\n(2) uproszczonej metody Newtona\n(3) metody siecznych\n";
            cin >> method;
            switch (method)
            {
                case 1:
                    newtonsMethod();
                    break;
                case 2:
                    simplifiedNewtonsMethod();
                    break;
                case 3:
                    secantMethod();
                    break;
                default:
                    newtonsMethod();
                    break;
            }
        }
        if (nr <= 10)
        {
            cout << "Uzyc wzoru Aitkena: (1) standardowego, czy (2) numerycznie bardziej poprawnego?\n";
            cin >> mode;
            mode == 1 ? aitkensDeltaSquared() : improvedAitkensMethod();
            cout << "Ile cyfr po przecinku wypisac?\n";
            cin >> precision;
            printResults(precision, result);
        }
    }
}