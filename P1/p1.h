#ifndef ANALIZA_NUMERYCZNA_P1
#define ANALIZA_NUMERYCZNA_P1

const long double FI = 1.61803398874989484820458683436564L;
const long double E = 2.71828182845904523536028747135266L;
const long double PI = 3.14159265358979323846264338327950L;
const long double PI_4 = 0.78539816339744830961566084581988L;
const long double ZERO_OF_POLYNOMIAL = 1.85558452864093786376025L;
const long double ZERO_OF_F = 0.51493326466112941380105925843691L;
const long double SUM_OF_B_SERIES = 2.612375348685488L;
const long double DERRIVATIVE_OF_F_0 = 1.8995672244502337L;

long double aSeries();
long double bSeries();
long double cSeries();
long double dSeries();
long double fibonacciNumbers();
long double wallisNumbers();
long double stirlingNumbers();
long double F(long double x);
long double f(long double x);
void newtonsMethod();
void simplifiedNewtonsMethod();
void secantMethod();
long double banachFixedPointIteration();
long double steffensensMethod();
void aitkensDeltaSquared();
void improvedAitkensMethod();
void printResults(int precision, long double result);
void bSeriesToFile(const char *filename, long double result);
void cSeriesToFile(const char *filename, long double result);
void wallisNumbersToFile(const char *filename, long double result);
void saveFile(const char *filename, long double Tab100[50][100], long double result);
void aitkenDeltaSquared50Times(long double Tab100[50][100]);

#endif //ANALIZA_NUMERYCZNA_P1_H
