\documentclass[11pt,wide]{mwart}

\usepackage[OT4,plmath]{polski}
\usepackage[utf8]{inputenc}
\usepackage[polish]{babel}
\usepackage{amsmath, amsfonts}

\title{{\LARGE\textbf{Pracownia z analizy numerycznej}}\\
       {\Large Sprawozdanie z zadania \textbf{P1.10.}}\\
       {\large Prowadzący: dr hab. Paweł Woźny}}
\author{Łukasz Arczyński}
\date{Wrocław, \today\ r.}

\begin{document}
\maketitle

\setcounter{equation}{0}
\section{Wstęp}
Obliczanie granic ciągów (i szeregów) jest ważnym działem analizy matematycznej. Znajduje on zastosowanie w analizie numerycznej, fizyce i wielu innych dziedzinach nauki. Stosuje się go do rozwiązywania takich problemów jak znajdowanie miejsc zerowych pewnych funkcji (metoda Newtona, Banacha, stycznych), rozwiązywanie równań nieliniowych i układów równań liniowych, obliczania całek i pochodnych. \\
Często jednak obliczenie granicy z definicji może być bardzo trudne lub nawet niemożliwe. Czasami możemy nawet nie znać wzoru danego ciągu, a tylko kilka-kilkanaście jego początkowych wyrazów, każdy z nich może mieć nie więcej niż 2-3 cyfry znaczące.\\
W niniejszym sprawozdaniu przedstawię metodę $\Delta^2$ Aitkena, dzięki której w prosty sposób można obliczyć granicę ciągu o powyższych własnościach z dokładnością nawet kilkunastu cyfr znaczących. Przedstawię również niebezpieczeństwa wynikające z użycia tej metody. \\
Opisałem również inną metodę przyspieszania zbieżności, metodę Steffensena, która jest dokładniejsza i bezpieczniejsza od metody Aitkena, lecz może być użyta jedynie do niektórych z wymienionych zastosowań.\\
Testy zostały przeprowadzone za pomocą programu napisanego w języku C++, używając podwyższonej precyzji obliczeń (\texttt{long double}), więc dokładność będzie zawsze nie większa od 20.

\section{Zasada działania procesu $\Delta^2$ Aitkena i metody Steffensena}
\noindent Z definicji liniowej zbieżności ciągu:
$$
\lim_{n\to\infty} \frac{|s_{n+1}-s|}{|s_{n}-s|}      =    \lambda,
$$
gdzie 
$$
s = \lim_{n\to\infty}s_n.
$$
Więc dla dostatecznie dużych n
$$
\frac{|s_{n+1}-s|}{|s_{n}-s|}  \approx  \lambda.
$$
Wtedy
$$
\frac{s_{n+2}-s}{s_{n+1}-s}  \approx  \frac{s_{n+1}-s}{s_{n}-s}     \iff     ({s_{n+2}-s})({s_{n}-s}) \approx ({s_{n+1}-s})^2
$$
$$
s_{n}s_{n+2}-s_{n+1}^2 \approx s(s_{n+2}-2s_{n+1}+s_{n}).
$$
Otrzymujemy
\begin{equation}
s \approx \frac{s_{n}s_{n+2}-s_{n+1}^2}{s_{n+2}-2s_{n+1}+s_{n}}.
\end{equation}
Co, po przekształceniu można zapisać jako
\begin{equation}\label{Aitken}
s \approx s_{n} - \frac{(s_{n+1}-s_{n})^2}{s_{n+2}-2s_{n+1}+s_{n}} =  s_{n}-\frac{(\Delta s_{n})^2}{\Delta^2 s_{n}}
\end{equation}
dla
$$
\Delta s_{n}:=s_{n+1}-s_{n}
$$
$$
\Delta^2 s_{n}:=\Delta(\Delta s_{n})=\Delta s_{n+1}-\Delta s_{n}=(s_{n+2}-s_{n+1})-(s_{n+1}-s_{n})=s_{n+2}-2s_{n+2}+s_{n}
$$
Można więc zdefiniować ciąg 
\begin{equation}
 t_{1_n} :=  s_{n} - \frac{(s_{n+1}-s_{n})^2}{s_{n+2}-2s_{n+1}+s_{n}},
\end{equation}
lub ogólniej
\begin{equation}
 t_{k+1_n} := t_{k_{n}} - \frac{(t_{k_{n+1}}-t_{k_{n}})^2}{t_{k_{n+2}}-2t_{k_{n+1}}+t_{k_{n}}},
\end{equation}


Ze wzorów (2), (3) i (4) wynika, że $t_{1_n}$ i $t_{k+1_n}$ (dla każdego $k>1$) zbiegają do $s$ przy $n\to\infty$. Co więcej, dla każdego $k>0$, ciąg $t_{k+1_n}$ jest zbieżny do $s$ szybciej niż ciąg $t_{k_n}$. Obserwacji tej można użyć przyspieszania zbieżności ciągu $s_n$, co pozwoli na szybsze numeryczne wyznaczanie wartości $s$. Metoda ta nazywa się procesem $\Delta^2$ Aitkena. Do obliczenia wartości elementu $t_{k_1}$ należy obliczyć wartości
\begin{equation}
(k+1)+\sum_{i=1}^{k}2i=(k+1)^2
\end{equation}
elementów.\\
Procesu Aitkena można użyć również do przyspieszania zbieżności numerycznych metod wyznaczania miejsc zerowych funkcji, takich jak metoda Newtona lub metoda siecznych, lub metoda Banacha (zwana też metodą iteracji punktu stałego). Inną metodą wyznaczania miejsc zerowych, którą opisałem jest metoda Steffensena. \\
Aby pokazać zasadę działania metody Steffensena, należy poznać metodę Banacha wyznaczania miejsca zerowego funkcji rzeczywistej, jednej zmiennej.
Aby wyznaczyć miejsce zerowe $\alpha$ funkcji $f$, używając tej metody, należy przekształcić równanie $f(x)=0$ do postaci $x=g(x)$. Następnie, mając dane $x_0$, będące przybliżeniem $\alpha$, obliczamy kolejne przybliżenia $\alpha$ według wzoru $x_k=g(x_{k-1})$, dla $k>0$. Można wykazać, że dla odpowiednio wybranej funkcji $g$ i początkowego przybliżenia $x_0$, $\displaystyle \lim_{n\to\infty} x_n=\alpha$. \\
Metoda Steffensena polega na obliczeniu wartości $x_1$ i $x_2$, używając metody Banacha, a następnie obliczeniu $x_3$ metodą Aitkena, korzystając z wartości $x_1$ i $x_2$. Proces można kontynuować, licząc $x_{3k-2}$ i $x_{3k-1}$ metodą Banacha, a $x_{3k}$ metodą Aitkena (dla $k>1$). Jak zobaczymy w podrozdziale 3.2, metoda ta jest w pewnych przypadkach bardziej skuteczna od wielokrotnego stosowania procesu Aitkena do wartości $x_n$ obliczonych za pomocą metody Banacha.


\section{Wyniki wybranych testów działania metod Aitkena i Steffensena}
\subsection{Obliczanie wyrazów ciągów zbieżnych}
Jak wiadomo z rozdziału 2., metoda Aitkena działa dla ciągów zbieżnych liniowo. Ciąg $s_n=\frac{F_{n+1}}{F_n}$, gdzie $F_k$ jest $k$-tą liczbą Fibonacciego jest liniowo zbieżny do $\varphi \approx 1,6180339887$. W Tablicy 1. przedstawione są wartości 

\begin{equation}
\text{dokładność}:=-\log_{10}{r},\text{gdzie}\  r=|t_{k_n}|, \text{a } t_{0_n}:=s_n. 
\end{equation}

Przy czym wartość $\inf$ oznacza, że dokładność jest większa od $20$, czyli błąd jest mniejszy od $10^{-20}$, czyli użytej precyzji arytmetyki.

\begin{table}[!h]
\renewcommand{\arraystretch}{1.5}
\caption{Dokładność przybliżeń liczby $\varphi$ przez $s_n$ i $t_{k_n}$ $ \displaystyle \left( s_n = \frac{F_{n+1}}{F_n}\right) $.}
\begin{center}
\begin{tabular}{|c||c|c|c|c|c|} \hline
n &  $s_n$ & $t_{1_n}$ & $t_{2_n}$ & $t_{3_n}$ & $t_{4_n}$  \\ \hline \hline
1 & 0.21 & 1.31 & 4.67 & 13.03 & 18.96 \\ \cline{1-6}
2 & 0.42 & 2.16 & 6.34 & 16.37 & \ inf  \\ \cline{1-6}
3 & 0.93 & 2.99 & 8.01 & 18.96  \\ \cline{1-5}
4 & 1.31 & 3.83 & 9.68 & \ inf  \\ \cline{1-5}
5 & 1.74 & 4.67 & 11.35  \\ \cline{1-4}
6 & 2.16 & 5.50 & 13.03  \\ \cline{1-4}
7 & 2.58 & 6.34   \\ \cline{1-3}
8 & 2.99 & 7.17 \\  \cline{1-3}
9 & 3.41 \\ \cline{1-2}
10 & 3.83  \\ \cline{1-2}
\end{tabular}
\end{center}
\end{table}
\renewcommand{\arraystretch}{1}

W Tablicy 1. widać, że dokładność znacznie się zwiększa po każdorazowym zastosowaniu metody Aitkena. Prawie każdy element $t_{k_n}$ jest większy od największego z elementów użytych do jego obliczenia $\left(t_{{k-1}_{n}}, t_{{k-1}_{n+1}}, t_{{k-1}_{n+2}}\right)$. Wyjątkiem jest element $t_{4_1}$, który jest równy elementowi $t_{3_3}$. Można zauważyć, że dla każdego k ciągi $t_{k_n}$ są zbieżne do $\varphi$ liniowo.

Metoda Aitkena działa wyjątkowo dobrze dla ciągów naprzemiennych. W Tablicy 2. są przedstawione rzeczywiste wartości  przybliżeń liczby $\displaystyle \frac{\pi}{4}$ przez ciąg naprzemienny  $s_n = \sum_{j=0}^{n} (2j+1)^{-1}$ (zbieżny do $\frac{\pi}{4}\approx 0,78539816$) i przez odpowiadający mu ciąg $t_{1_n}$. Jak łatwo zauważyć, $t_{1_n} \approx t_{1_{n+1}}$, mimo, że zależność taka nie zachodzi dla ciągu $s_n$. Dokładności przybliżeń dla przedstawionych wyrazów ciągów $s_n$ i $t_{1_n}$ nie większe od odpowiednio 1,5 i 4.

\begin{table}[!h]
\renewcommand{\arraystretch}{1.5}
\caption{Rzeczywiste wartości przybliżeń liczby $\displaystyle \frac{\pi}{4}$ przez $s_n$ i $t_{1_n}$ $ \displaystyle \left( s_n = \sum_{j=0}^{n} (2j+1)^{-1}\right) $.}
\begin{center}
\begin{tabular}{|c||c|c|c|c|c|c|c|c|} \hline
n & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7  \\ \hline \hline
$s_n$ & 1.0000 & 0.6667 & 0.8667 & 0.7238 & 0.8349 & 0.7440 & 0.8209 & 0.7543 \\ \hline
$t_{1_n}$ & 0.7917 & 0.7833 & 0.7863 & 0.7849 & 0.7857 & 0.7852 \\ \cline{1-7}

\end{tabular}
\end{center}
\end{table}
\renewcommand{\arraystretch}{1}

Okazuje się jednak, że metoda Aitkena może być z powodzeniem użyta w przypadku ciągów zbieżnych wolniej lub szybciej niż liniowo. W Tablicy 3 i 4 są przedstawione częściowe wyniki (pełne wyniki są zapisane w pliku wyniki.txt) użycia metody Aitkena dla ciągu 
$$s_n = \frac{((2n)!!)^2}{(2n-1)!!(2n+1)!!}, \text{(wzór Wallisa)}$$ 
 zbiegającego do $ \pi$ wolniej niż liniowo i ciągu 
$$ s_n = \sum_{i=0}^{n} \frac{1}{i!},$$
 zbiegającego do $e$ szybciej niż liniowo.

\begin{table}[!h]
\renewcommand{\arraystretch}{1.5}
\caption{Dokładność przybliżeń liczby $ \displaystyle  \pi$ przez $s_n$ i $t_{k_n}$ $ \displaystyle \left( s_n = \frac{((2n)!!)^2}{(2n-1)!!(2n+1)!!}\right) $.}
\begin{center}
\begin{tabular}{|c||c|c|c|c|c|c|c|c|c|c|} \hline
n &  $s_n$ & $t_{1_n}$ & $t_{2_n}$ & $t_{3_n}$ & $t_{4_n}$ & $t_{5_n}$ & $t_{6_n}$ & $t_{7_n}$ & $t_{8_n}$ & $t_{9_n}$  \\ \hline \hline
1  & 0.32  & 0.83  & 1.27  & 1.68  & 2.08  & 2.65  & 2.57  & 2.56  & 3.12  & 3.06  \\ \cline{1-11}
2  & 0.53  & 0.97  & 1.38  & 1.76  & 2.14  & 2.55  & 2.50  & 3.60  & 3.06  & 3.06  \\ \cline{1-11}
3  & 0.67  & 1.07  & 1.46  & 1.83  & 2.20  & 2.58  & 3.02  & 3.05  & 3.06  \\ \cline{1-10}
4  & 0.77  & 1.16  & 1.53  & 1.89  & 2.25  & 2.61  & 3.13  & 3.06  & 3.06  \\ \cline{1-10}
5  & 0.86  & 1.23  & 1.59  & 1.95  & 2.30  & 2.65  & 3.06  & 3.06  \\ \cline{1-9}
6  & 0.93  & 1.29  & 1.64  & 1.99  & 2.34  & 2.68  & 3.06  & 2.92  \\ \cline{1-9}
7  & 0.99  & 1.34  & 1.69  & 2.04  & 2.38  & 2.72  & 3.08  \\ \cline{1-8}
8  & 1.04  & 1.39  & 1.73  & 2.07  & 2.41  & 2.75  & 3.10  \\ \cline{1-8}

\end{tabular}
\end{center}
\end{table}
\renewcommand{\arraystretch}{1} 

W obu przypadkach zastosowanie metody Aitkena poprawiło szybkość zbieżności ciągów, lecz w obu tablicach, od pewnego momentu dokładność przestaje rosnąć, a nawet zaczyna maleć. Efekt utraty dokładności omówię na przykładzie Tablicy 4, gdyż, z powodu szybszej zbieżności ciągu, jest tam lepiej widoczny.

Można zauważyć, że jeśli element $t_{k_n}$ został utworzony z elementów $t_{{k-1}_n}$,$ t_{{k-1}_{n+1}}$ i $t_{{k-1}_{n+2}}$, takich, że 
\begin{equation}
t_{{k-1}_n} \approx t_{{k-1}_{n+1}} \text{ i } t_{{k-1}_{n+1}} \text{ jest znacząco mniejszy od }  t_{{k-1}_{n+2}},
\end{equation}
wtedy obliczony element $t_{k_n} \approx t_{{k-1}_n}$, czyli  $t_{k_n}$ jest znacznie mniejszy od $ t_{{k-1}_{n+2}}$. Efekt ten jest spowodowany utratą cyfr znaczących podczas odejmowania wyrazów ciągów $t_{{k-1}_n}$ i $t_{{k-1}_{n+1}}$ zgodnie z wzorem (4).

Gdy zamiast wzoru (4) zastosujemy wzór (1), dokładność w powyższym przypadku drastycznie maleje. Dokładność wyrazu $t_{k_n}$, dla $k\geq8$, w większości przypadków nie przekracza 10.

\begin{table}[!h]
\renewcommand{\arraystretch}{1.5}
\caption{Dokładność przybliżeń liczby $e$ przez $s_n$ i $t_{k_n}$ $ \displaystyle \left( s_n = \sum_{i=0}^{n} \frac{1}{i!} \right) $.}
\begin{center}
\begin{tabular}{|c||c|c|c|c|c|c|c|c|c|} \hline
n &  $s_n$ & $t_{1_n}$ & $t_{2_n}$ & $t_{3_n}$ & $t_{4_n}$ & $t_{5_n}$ & $t_{6_n}$ & $t_{7_n}$ & $t_{8_n}$  \\ \hline \hline
1 &-0.24 & 0.55 & 3.33 & 5.20 & 7.15 & 11.32 & 14.50 & 16.21 & 16.20\\ \hline
2 & 0.14 & 1.50 & 4.56 & 6.17 & 9.44 & 13.53 & 16.30 & 16.28 \\ \cline{1-9}
3 & 0.66 & 2.40 & 5.27 & 7.87 & 11.18 & 14.52 & 16.21 & 18.36 \\ \cline{1-9}
4 & 1.29 & 3.33 & 6.24 & 9.21 & 13.05 & 15.58 & 17.62  \\ \cline{1-8}
5 & 2.00 & 4.29 & 7.28 & 10.47 & 14.77 & 16.57 & 18.66 \\ \cline{1-8}
6 & 2.79 & 5.28 & 8.37 & 11.71 & 15.62 & 18.66 \\ \cline{1-7}
7 & 3.65 & 6.31 & 9.50 & 12.95 & 16.81 & 18.66 \\ \cline{1-7}
8 & 4.56 & 7.38 & 10.67 & 14.21 & 18.06 \\ \cline{1-6}

\end{tabular}
\end{center}
\end{table}
\renewcommand{\arraystretch}{1}

 Efekt utraty dokładności przy stosowaniu wzoru (4) jest jeszcze lepiej widoczny przy wielokrotnym stosowaniu metody Aitkena. W pliku sto.txt przedstawione są wyniki  pięćdziesięciokrotnego zastosowania metody Aitkena na 100 początkowych wyrazach ciągu 
$$ s_n = \left(1+\frac{1}{n}\right)^n.$$
Dokładność wyrazu $t_{49_1}$ to $2,77$, podczas, gdy największa dokładność to $3,85$ osiągnięta w wyrazie $t_{10_{57}}$. 

Jeśli natomiast 
\begin{equation}
t_{{k-1}_n} \approx t_{{k-1}_{n+1}} \approx t_{{k-1}_{n+2}} \text{(z dokładnością do precyzji arytmetyki),}
\end{equation}
wtedy $t_{k_n} = t_{k_n} - \frac{0}{0}$, co jest wyrażeniem nieoznaczonym, a w C++ jest zapisywane jako nan. Ponieważ $ \forall{x \in \mathbb{R}}\forall{\oplus \in \{+,-,*,/\}} \text{ nan} \oplus x = \text{nan} $ i $x \oplus \text{nan} = \text{nan}$, każdy wyraz, do którego obliczenia będzie użyty wyraz nan, również będzie nan. Wyniki użycia metody Aitkena na ciągach, których pewne wyrazy spełniają warunek (6), można zobaczyć w plikach wyniki.txt i sto.txt.

W pliku stodwa.txt przedstawione są wyniki  pięćdziesięciokrotnego zastosowania metody Aitkena na 100 początkowych wyrazach ciągu
$$s_n = \sum_{k=1}^{n} k^{-\frac{3}{2}}, \text{zbieżnego wolniej niż liniowo do } \zeta\left(\frac{3}{2}\right) \approx 2,61237534.$$
Możemy zobaczyć tam bardzo dużą utratę dokładności wynikającą z innego powodu. Dokładność elementu $t_{15_9}$ jest równa $-0,34$, mimo, że dokładności elementów użyte do jego obliczenia są większe od $1,25$. Sprawdziłem więc rzeczywiste wartości $t_{14_9}$, $t_{14_10}$ i $t_{14_11}$. W przybliżeniu, były one równe odpowiednio $2,55794745$, $2.57256912$ i $2.58729017$. Po podstawieniu tych wartości do wzoru (4), otrzymujemy 
$$t_{15_9} = 2,55794745 - \frac{(2,55794745 - 2,57256912)^2}{2,58729017 - 2*2,57256912 + 2,55794745} = 0.40657158.$$
Metoda Aitkena zawiodła.

\subsection{Wyznaczanie miejsc zerowych funkcji}

Metody Aitkena można z powodzeniem używać również do przyspieszania zbieżności numerycznych metod wyznaczania miejsc zerowych funkcji rzeczywistych jednej zmiennej. W Tablicy 5. przedstawione są częściowe wyniki użycia metody Aitkena na ciągu kolejnych przybliżeń miejsca zerowego funkcji $f(x)=x^4-x-10$ za pomocą metody Banacha (pełne wyniki, jak i wyniki innych testów są dostępne w pliku wyniki.txt).

\begin{table}[!h]
\renewcommand{\arraystretch}{1.5}
\caption{Dokładność przybliżeń miejsca zerowego funkcji $f(x)=x^4-x-10$ za pomocą metody Banacha.}
\begin{center}
\begin{tabular}{|c||c|c|c|c|c|c|c|c|c|} \hline
n &  $x_n$ & $t_{1_n}$ & $t_{2_n}$ & $t_{3_n}$ & $t_{4_n}$ & $t_{5_n}$ & $t_{6_n}$ & $t_{7_n}$ & $t_{8_n}$  \\ \hline \hline
1 & 1.26  & 3.12  & 6.13  & 7.16  & 8.15  & 9.32  & 10.52  & 11.93  & 13.42  \\ \hline
 2 & 1.28  & 3.19  & 6.36  & 7.30  & 8.43  & 9.53  & 10.84  & 12.21    \\ \cline{1-9}
 3 & 1.32  & 3.26  & 6.35  & 7.45  & 8.51  & 9.75  & 11.02  & 12.50   \\ \cline{1-9}
 4 & 1.35  & 3.33  & 6.56  & 7.59  & 8.78  & 9.96  & 11.33    \\ \cline{1-8}
 5 & 1.39  & 3.40  & 6.57  & 7.73  & 8.87  & 10.17  & 11.52   \\ \cline{1-8}
\end{tabular}
\end{center}
\end{table}
\renewcommand{\arraystretch}{1}

Element $t_{8_1}$ ma dokładność 13,42. Obliczenie jego wartości wymagało obliczenia wartości 81 elementów (ze wzoru (5).).

W Tablicy 6. przedstawione są wyniki użycia metody Steffensena (opisanej w rozdziale 2.) do wyznaczenia miejsca zerowego tej samej funkcji $\left(f(x)=x^4-x-10\right)$.

\begin{table}[!h]
\renewcommand{\arraystretch}{1.5}
\caption{Dokładność przybliżeń miejsca zerowego funkcji $f(x)=x^4-x-10$ za pomocą metody Steffensena.}
\begin{center}
\begin{tabular}{|c||c|c|c|c|c|c|c|c|c|c|c|c|c|} \hline
n & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12  \\ \hline \hline
$x_n$ & 1.26  & 1.28  & 1.32  & 3.12  & 3.16  & 3.19  & 6.87  & 6.90  & 6.94  & 14.36  & 14.39  & 14.43 \\ \hline
\end{tabular}
\end{center}
\end{table}
\renewcommand{\arraystretch}{1}
Jak widzimy, element $x_{12}$ ma dokładność 14,43. Do obliczenia jego wartości należało obliczyć wartości jedynie 11 elementów. Metoda Steffensena jest dużo szybciej zbieżna od metody przedstawionej w Tablicy 6., mimo, że obie metody wykorzystują metody Banacha i Aitkena. Metoda Steffensena, w przeciwieństwie do metody Aitkena poprawia zbieżność metody Banacha z liniowej na kwadratową.

Metoda Aitkena może być również użyta dla ciągu $x_n$ przedstawionego w Tablicy 6. Niestety, po kilkukrotnym użyciu, spowoduje ona znaczny spadek dokładności, ponieważ wiele wyrazów ciągu $x_n$ spełnia zależność podaną we wzorze (7). Wyniki użycia tej metody na ciągu $x_n$ można znaleźć z pliku wyniki.txt.

Jak więc widzimy, w większości przypadków metoda Aitkena poprawia zbieżność najlepiej, gdy jest użyta niewiele razy. Po kolejnych użyciach tej metody przyspieszenie zbieżności jest mniejsze, a od pewnego momentu niemal niezauważalne. Jeśli kolejne wyrazy ciągu mają podobne wartości, użycie metody Aitkena może spowodować utratę dokładności, a nawet wystąpienie wartości nan. Po kilkunastokrotnym użyciu tej metody często zdarza się, że dokładność nagle spada, z przyczyn nie związanych z dokładnością arytmetyki lub utratą cyfr znaczących, lecz prawdopodobnie z przybliżeniem występującym we wzorach (1) i (2). Jednak dla dużej liczby ciągów, zwłaszcza ciągów naprzemiennych metoda Aitkena daje duży przyrost dokładności i pozwala otrzymać żądaną dokładność po obliczeniu kilkudziesięciu wyrazów, zamiast kilku tysięcy (np. dla ciągu $s_n=(1+\frac{1}{n})^n$). Dlatego, chcąc wyznaczyć granicę pewnego zbieżnego ciągu można używać procesu Aitkena, lecz nie więcej niż 3-5 razy.

Jeśli dany ciąg jest ciągiem przybliżeń miejsca zerowego pewnej funkcji, wtedy warto użyć metody Steffensena, która w wielu przypadkach jest szybsza i bezpieczniejsza od metody Aitkena. Co więcej, w przeciwieństwie do metody Aitkena, daje nam pewność, że dokładność ostatniego obliczonego wyrazu będzie większa niż wszystkich poprzednich.

\end{document}

