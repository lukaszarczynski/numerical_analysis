\documentclass[11pt,wide]{mwart}

\usepackage[OT4,plmath]{polski}
\usepackage[utf8]{inputenc}
\usepackage[polish]{babel}
\usepackage{amsmath, amsfonts}
\usepackage{graphicx}
\usepackage{epstopdf}

\title{{\LARGE\textbf{Pracownia z analizy numerycznej}}\\
       {\Large Sprawozdanie z zadania \textbf{P3.4.}}\\
       {\large Prowadzący: dr hab. Paweł Woźny}}
\author{Łukasz Arczyński\thanks{\textit{Nr indeksu}: \texttt{258424}}}
\date{Wrocław, \today\ r.}

\let\oldsqrt\sqrt
\def\sqrt{\mathpalette\DHLhksqrt}
\def\DHLhksqrt#1#2{%
\setbox0=\hbox{$#1\oldsqrt{#2\,}$}\dimen0=\ht0
\advance\dimen0-0.2\ht0
\setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
{\box0\lower0.4pt\box2}}

\begin{document}
\maketitle

\setcounter{equation}{0}
\section{Wstęp}
Obliczanie pojedyńczych całek oznaczonych jest ważnym działem analizy matematycznej. Znajduje on zastosowanie w analizie numerycznej, fizyce i wielu innych dziedzinach nauki. Jest używane do wyznaczania wartości oznaczonych całek podwójnych, potrójnych, krzywoliniowych i powierzchniowych.

Niestety, obliczanie całek poprzez znalezienie funkcji pierwotnej jest często bardzo trudne. Nie ma algorytmu pozwalającego wyznaczyć całkę nieoznaczoną z danej funkcji. Co więcej wielu całek nieoznaczonych (np. $e^{-x^2}$) nie można wyrazić za pomocą funkcji elementarnych.

Na szczęście, w wielu przypadkach nie musimy znać dokładnej wartości całki, wystarczające będzie pewne jej przybliżenie. W niniejszym sprawozdaniu przedstawię pewną metodę przybliżania wartości całki i omówię sposoby zwiększenia dokładności tego przybliżenia.

Testy zostały przeprowadzone za pomocą programu napisanego w języku C++, używając podwójnej precyzji obliczeń (\texttt{double}).
\section{Kwadratury interpolacyjne}
Kwadratura interpolacyjna jst sposobem przybliżania wartości całki $\int_a^bp(x)f(x)dx$ za pomocą całki $\int_a^bp(x)L_n(x)dx$, gdzie $L_n(x)$ jest wielomianem interpolacyjnym Lagrange'a interpolującym funkcję $f(x)$ w punktach $x_0,x_1,\ldots,x_n$, danym wzorem
\begin{equation}
L_n(x) = \sum_{k=0}^nf(x_k)\lambda_k(x),
\end{equation}
i spełniającym zależność
$$
f(x) = L_n(x) + r_n(x),
$$
gdzie $r_n(x)$ jest resztą wielomianu interpolacyjnego.
\subsection{Kwadratura Newtona-Cotesa}
Kwadratura Newtona-Cotesa to taka kwadratura interpolacyjna, w której węzły są równoodległe
$$
x_k=a+kh
$$
Stosuje się ją do obliczania całek
$$
I(f) = \int_a^b f(x) dx
$$
Za pomocą wzoru
\begin{equation}
Q_n(f) = \sum_{k=0}^nA_kf(x_k)
\end{equation}
Gdzie
\begin{equation}
A_k=\int_a^b \lambda_k(x)
\end{equation}
$$
\lambda_k(x) = \frac{\omega(x)}{\omega'(x_k)(x-x_k)}
$$
$$
\omega(x) = \prod_{i=0}^n(x-x_i)
$$
$$
\omega'(x_k) = \prod_{\substack{i=0\\i\neq k}}^n(x_k-x_i)
$$
Można zauważyć, że skoro $x_k = a+ kh$, to
$$
\omega(x) = h^{n+1}\prod_{i=0}^n(t-i)
$$
$$
\omega'(x_k) = (-1)^{n-k}h^nk!(n-k!)
$$
Więc
$$
\lambda_k(x) = \frac{h(-1)^{n-k}}{k!(n-k)!}\prod_{\substack{i=0\\i\neq k}}^n(t-i)
$$
Czyli
\begin{equation}
A_k = \frac{h(-1)^{n-k}}{k!(n-k)!}\int_0^n\prod_{\substack{i=0\\i\neq k}}^n(t-i)dt
\end{equation}
$$
A_k=A_{n-k}
$$
\subsection{Kwadratury Gaussa}
Kwadratury Gaussa stosuje się do obliczania całek
\begin{equation}
I(f) = \int_a^b p(x)f(x) dx
\end{equation}
Za pomocą wzoru
\begin{equation}
Q_n(f) = \sum_{k=0}^nA_kf(x_k)
\end{equation}
gdzie $x_k$ jest $k$-tym zerem $n+1$-go wielomianu ortogonalnego z wagą $p$, a wagi $A_k$ dane są wzorem
$$
A_k = \int_{a}^{b}p(x)\lambda_k(x)dx
$$
$$
\lambda_k=\frac{\omega(x)}{\omega'(x_k)(x-x_k)}=\frac{\overline{P}_{n+1}(x)}{(x-x_k)\overline{P'}_{n+1}(x_k)}
$$
$$
\overline{P}_{n+1}(x)=\frac{P_{n+1}(x)}{a_{n+1}}=\omega(x)=(x-x_0)(x-x_1)\ldots(x-x_n) % Zamiast mnożyć (x-x_0)... mogę użyć P_{n+1}(x) z poprzedniej pracowni
$$
$$
P_n(x) = a_nx^n+a_{n-1}x^{n-1}+\ldots+a_1x+a_0
$$
Współczynniki $A_k$ można więc wyrazić wzorem
\begin{equation}
A_k = \frac{a_{n+1}}{a_n}\frac{1}{P'_{n+1}(x_k)} \int_a^bp(x)\frac{\overline{P}_{n+1}(x)}{x-x_k}dx = \frac{a_{n+1}}{a_n}\frac{||P_n||^2}{P'_{n+1}(x_k)P_n(x_k)}
\end{equation}

Aby sprawdzić, czy błąd przybliżenia jest wynikiem złej interpolacji funkcji przez wielomian interpolacyjny, czy wynikiem błędów numerycznych przy obliczaniu współczynników $A_k$ można użyć wzoru na resztę kwadratury Gaussa
\begin{equation}
R_n(f) \leq \frac{\max_{a\leq x\leq b}f^{(2n+2)}(x)}{(2n+2)!a^2_{n+1}}\int_a^bp(x)(\overline{P}_{n+1}(x))^2 dx.
\end{equation}
\subsection{Kwadratura Gaussa-Czebyszewa}
Kwadratura Gaussa-Czebyszewa to kwadratura interpolacyjna z wagą $p(x)=\frac{1}{\sqrt{1-x^2}}$ wykorzystywana do obliczania całek
\begin{equation}
I(f) = \int_{-1}^1 \frac{f(x)}{\sqrt{1-x^2}} dx.
\end{equation}
Jest określona wzorem
$$
Q_n(f) = \sum_{k=0}^nA_kf(x_k),
$$
$x_k$ jest $k$-tym miejscem zerowym $n+1$-go wielomianu Czebyszewa, a wagi $A_k$ są równe
\begin{equation}
A_k = \frac{||P_n||^2}{P'_{n+1}(x_k)P_n(x_k)} = \frac{\pi}{n+1}
\end{equation}
\subsection{Kwadratura Gaussa-Legendre'a}
Kwadratura Gaussa-Legendre'a to kwadratura interpolacyjna z wagą $p(x)\equiv1$ wykorzystywana do obliczania całek
$$
I(f) = \int_{-1}^1 f(x) dx.
$$
Jest określona wzorem
$$
Q_n(f) = \sum_{k=0}^nA_kf(x_k),
$$
gdzie $x_k$ jest $k$-tym miejscem zerowym $n+1$-go wielomianu Legendre'a $P_{n+1}(x)$, a $A_k$ wyraża się wzorem
$$
A_k=\frac{\int_{-1}^1\overline{P^2_{n+1}}(x)dx}{\overline{P'}_{n+1}(x_k)\overline{P}_n(x_k)}=\frac{2}{(1-x_k^2)(P'_n(x_k))^2},
$$
A wielomiany $\overline{P}_k$ spełniają zależność rekurencyjną
$$
\overline{P}_0(x)\equiv1, \overline{P}_1(x)=x
$$
$$
\overline{P}_{k+1}(x)=x\overline{P}_k(x)-\frac{k^2}{4k^2-1}\overline{P}_{k-1}(x)
$$
Aby obliczyć całkę w przedziale $[a,b]$ za pomocą kwadratury Gausa-Legendre'a, musi ona zostać przedstawiona za pomocą całki w przedziale $[-1,1]$ za pomocą wzoru
$$
\int_a^bf(x)dx = \frac{b-a}{2}\int_{-1}^1f\left(\frac{b-a}{2}x+\frac{a+b}{2}\right)dx.
$$
\section{Analiza wybranych wyników testów}
Niech $E_n(f)$ będzie błędem przedstawienia całki $I(f)$ za pomocą kwadratury $Q_n(f)$. Wtedy dokładnością będziemy nazywać wartość $-log_{10}(E_n(f))$.

W tym dziale przedstawię przybliżenia pewnych całek wybranymi kwadraturami i wniosek dotyczący dokładności tych kwadratur.

Dla całki
$$
\int_{-1}^1\frac{4}{\pi(1+x^2)} dx = 2
$$
wartość kwadratury Newtona-Cotesa $Q_8^{NC}$ jest równa $1.99927948908076$, więc jej dokładność wynosi $3.142$. Gdy zamiast $Q_8^{NC}$ użyjemy  $Q_{16}^{NC}$, dokładność rośnie do $5.199$. Gdy współczynniki $A_k$ obliczymy, używając wzoru (4) zamiast wzoru (3), dokładność nieznacznie spadnie, do $5.122$

Przy obliczaniu całki 
$$
\int_{-1}^1\frac{2\pi^2(1+x)}{(1-x)(3+x)} dx \to \infty
$$
kwadratura Newtona-Cotesa i kwadratura interpolacyjna wykorzystująca punkty ekstremalne wielomianu Czebyszewa przy $n=8$ jako wynik zwracają \texttt{inf}, czyli symbol w C++ oznaczający $\infty$. Kwadratura Gaussa-Czebyszewa przy takiej samej wartości $n$ jest równa $66,35348$.

Gdy szukana całka to
$$
\int_{-1}^1\frac{1}{x^4+x^2+0.9} dx,
$$
a $n=10$, dokładność kwadratury Newtona-Cotesa to $3.404$, kwadratury interpolacyjnej używającej punktów ekstremalnych wielomianu Czebyszewa to $ 5.697$, natomiast kwadratury Gaussa-Czebyszewa $5.526$. Natomiast, gdy przy obliczaniu wartości kwadratury Gaussa-Czebyszewa zamiast wzoru (3)  użyjemy wzoru (9), dokładność spada do $2.635$.

Natomiast przybliżając całkę
$$
\int_{-1}^1\frac{1}{1-x^2},
$$
gdy dla kwadratury Czebyszewa z $n=12$ użyjemy wzoru (9) zamiast wzoru (3), dokładność rośnie z $1.273$ do $15.051$.

Pełne wyniki dostępne są w pliku \texttt{wyniki.txt}.

Niestety, dla dużych wartości $n$, dokładność kwadratur zmniejsza się, najczęściej z powodów numerycznych. Dlatego w następnych działach zbadam sposoby poprawienia zbieżności kwadratur.

\section{Przyspieszanie zbieżności ciągu kwadratur metodą Aitkena}
W wielu przypadkach, kwadratura $Q_n$ zbliża się do całki tylko do określonego $n$, potem, z powodu stabilności numerycznej lub innych, dokładność się pogarsza. Można jednak spróbować otrzymać dokładniejsze przybliżenie stosując metodę Aitkena.

Mając ciąg elementów zbieżnych do całki (np. $Q_n(f), Q_{n+1}(f),\ldots,Q_{n+k}(f)$ dla pewnych kwadratur) można uzyskać dokładniejsze przybliżenie, korzystając ze wzoru
$$
 t_{k+1_n} := t_{k_{n}} - \frac{(t_{k_{n+1}}-t_{k_{n}})^2}{t_{k_{n+2}}-2t_{k_{n+1}}+t_{k_{n}}},
$$
gdzie $t_{0_n}$ jest n-tym elementem początkowego ciągu zbieżnego.

Przybliżając całkę $ \int_{-1}^1\frac{4}{Pi(1+x^2)}=2$ przez ciąg kwadratur Newtona-Cotesa, ciąg kwadratur $Q_n^{NC}$ zbliża się do wartości całki dla $n<20$. Dla wyższych $n$ dokładność zmniejsza się z powodu niewielkiej poprawności numerycznej użytego algorytmu. 
W Tablicy 1. przedstawiona jest dokładność przybliżeń całki $ \int_{-1}^1\frac{4}{Pi(1+x^2)}=2$ przez ciąg kwadratur Newtona-Cotesa, dla $n=2,\ldots,11$ i ciągi powstałe po wielokrotnym zastosowaniu metody Aitkena.

\begin{table}[!h]
\renewcommand{\arraystretch}{1.5}
\caption{Dokładność przybliżeń całki  $\displaystyle \int_{-1}^1\frac{4}{Pi(1+x^2)}=2$ przez ciąg kwadratur Newtona-Cotesa i ciągi powstałe po zastosowaniu metody Aitkena.}
\begin{center}
\begin{tabular}{|c||c|c|c|c|c|c|} \hline
n &  $Q_n^{NC}$ & $t_{1_n}$ & $t_{2_n}$ & $t_{3_n}$ & $t_{4_n}$ & $t_{5_n}$ \\ \hline \hline
2 & 0.91 & 1.04 & 1.54 & 1.47 & 2.04 & 1.83 \\ \cline{1-7}
3 & 1.42 & 2.12 & 1.70 & 2.51 & 2.30 & 2.90  \\ \cline{1-7}
4 & 1.86 & 1.44 & 2.28 & 2.08 & 2.69  \\ \cline{1-6}
5 & 2.18 & 2.77 & 2.54 & 3.14 & 2.94  \\ \cline{1-6}
10 & 3.69 & 3.69  \\ \cline{1-3}
11 & 3.93   \\ \cline{1-2}
\end{tabular}
\end{center}
\end{table}
\renewcommand{\arraystretch}{1}

Jak widać, użycie metody Aitkena nie przyniosło oczekiwanych rezultatów, wszystkie elementy powstałych ciągów mają mniejszą dokładność niż $Q_{11}^{NC}$.

Przy obliczaniu przybliżonej wartości całki $\int_{-1}^1\frac{1}{1+25x^2}$ poprzez ciąg kwadratur Gaussa-Czebyszewa, dokładność rośnie aż do $n=25$, gdzie osiąga maksymalną wartość $4.38$, po czym, dla $n=26$ dokładność gwałtownie spada, aż do $-5.46$. Jest to prawdopodobnie spowodowane tym, że pewien współczynnik używany do jej obliczania przekroczył zakres \texttt{double}.

Tablica 2. przedstawia dokładność przybliżeń całki $\int_{-1}^1\frac{1}{1+25x^2}$ przez ciąg kwadratur Gaussa-Czebyszewa, dla $n=20,\ldots,25$ i ciągi powstałe po wielokrotnym zastosowaniu metody Aitkena.

\begin{table}[!h]
\renewcommand{\arraystretch}{1.5}
\caption{Dokładność przybliżeń całki  $\displaystyle \int_{-1}^1\frac{1}{1+25x^2}$ przez ciąg kwadratur Gaussa-Czebyszewa i ciągi powstałe po zastosowaniu metody Aitkena.}
\begin{center}
\begin{tabular}{|c||c|c|c|c|} \hline
n &  $Q_n$ & $t_{1_n}$ & $t_{2_n}$ & $t_{3_n}$ \\ \hline \hline
20 & 3.5657 & 5.8746 & 5.4669 & 5.4166 \\ \cline{1-5}
21 & 3.6891 & 5.1306 & 5.0481 & 5.5457  \\ \cline{1-5}
22 & 3.8242 & 7.2323 & 6.6718   \\ \cline{1-4}
23 & 4.0536 & 5.3993 & 5.3999  \\ \cline{1-4}
24 & 4.2818 & 4.2817 \\ \cline{1-3}
25 & 4.3822    \\ \cline{1-2}
\end{tabular}
\end{center}
\end{table}
\renewcommand{\arraystretch}{1}

W tym przypadku, element $t_{1_22}$ osiąga dokładność $7,23$, czyli większą niż każda kwadratura $Q_n$ o 3 rzędy wielkości. Tym razem, po użyciu metody Aitkena zyskaliśmy 3 cyfry dokładne.

Przybliżając całkę $\int_{-1}^1e^{-x^2}$ przez ciąg kwadratur interpolacyjnych wykorzystujących punkty ekstremalne wielomianów Czebyszewa, dokładność rośnie do $n=17$, następnie zaczyna powoli spadać, z powodu poprawności numerycznej.

W Tablicy 3. przedstawiona jest dokładność przybliżeń całki $ \int_{-1}^1\frac{4}{Pi(1+x^2)}=2$ przez ciąg kwadratur interpolacyjnych opisanych wyżej, dla $n=2,\ldots,11$ i ciągi powstałe po wielokrotnym zastosowaniu metody Aitkena.

\begin{table}[!h]
\renewcommand{\arraystretch}{1.5}
\caption{Dokładność przybliżeń całki  $\displaystyle \int_{-1}^1e^{-x^2}$ przez ciąg kwadratur interpolacyjnych i ciągi powstałe po zastosowaniu metody Aitkena.}
\begin{center}
\begin{tabular}{|c||c|c|c|c|} \hline
n &  $Q_n$ & $t_{3_n}$ & $t_{4_n}$ & $t_{5_n}$ \\ \hline \hline
2 & 1.0709 & 5.8003 & 5.0959 & 7.7746 \\ \cline{1-5}
3 & 1.5628 & 4.7716 & 7.4417 & 8.3967  \\ \cline{1-5}
4 & 2.6256 & 6.6407 & 7.7656   \\ \cline{1-4}
5 & 4.0127 & 7.4688 & 9.5399  \\ \cline{1-4}
10 & 8.1723     \\ \cline{1-2}
11 & 8.6415     \\ \cline{1-2}
\end{tabular}
\end{center}
\end{table}
\renewcommand{\arraystretch}{1}

Element $t_{4_5}$ ma jest o rząd wielkości dokładniejszy, niż dowolny element ciągu $Q_n$.

Jak widzimy, poprawność działania metody Aitkena znacznie różni się w konkretnych przypadkach, ale często powala na dobre poprawienie dokładności przybliżenia szukanej całki.

Pełne wyniki użycia metody Aitkena są dostępne w pliku \texttt{aitken.txt}.

\section{Tablice współczynników $A_k$ i $x_k$}
Ponieważ wartości $A_k$ i $x_k$ nie zależą od funkcji $f$, można je stablicować. W tej części porównam dokładność kwadratury Gaussa-Legendre'a z stablicowanymi współczynnikami $A_k$ lub $x_k$ z kwadraturami, w których wszystkie współczynniki są obliczane przez program.

Wartości $x_k$ kwadratur badanych w tej części są miejscami zerowymi pewnych wielomianów ósmego stopnia. Okazuje się, że dla takich wielomianów błędy wyznaczania wartości $A_k$ i $x_k$ są rzędu $10^{-10}$, więc różnica pomiędzy wartościami wyznaczonymi i stablicowanymi jest widoczna, gdy wielomian interpolacyjny dobrze przybliża funkcję, czyli, gdy błąd kwadratury przy dokładnie wyznaczonych współczynnikach jest w tym przypadku nie większy niż $\approx10^{-10}$.

Dla funkcji $f(x) = e^{-x^2}$ dokładność kwadratury Gaussa-Legendre'adla $n=8$ przy współczynnikach wyznaczanych przez program jest równa $9.13912$. Gdy współczynniki pochodzą z wcześniej przygotowanej tablicy wynosi ona $9.13916$. Jak można zauważyć, niska dokładność jest skutkiem głownie wyboru punktów $x_k$, a w minimalnym stopniu błędami numerycznymi przy wyznaczaniu współczynników $A_k$.

Dla funkcji $f(x) = x^{73}$ dokładność kwadratury Gaussa-Legendre'adla $n=8$ przy współczynnikach wyznaczanych przez program jest równa $16.8987$. Dla stablicowanych współczynników zwiększa się ona do $19.2868$.

Dla funkcji $f(x) = sin(x)$ dokładność kwadratury Gaussa-Legendre'adla $n=8$ przy współczynnikach wyznaczanych przez program jest równa $13.8179$, dla współczynników odczytanych z tablicy $16.4953$.

Jak widać w dwóch powyższych przykładach, jeśli początkowa dokładność jest dość duża, może być ona zwiększona nawet 1000-krotnie, jesli użyje się stablicowanych współczynników $A_k$. Przy $n>8$ wzrost dokładności będzie jeszcze większy, gdyż błędy numeryczne obliczania $A_k$ wzrosną.

Pełne wyniki użycia stablicowanych współczynników są dostępne w pliki \texttt{stablicowane.txt}\\ \\
\indent Jak więc widzimy, przedstawione metody poprawiania dokładności kwadratur w wielu przypadkach mogą znacznie poprawić otrzymany wynik. Pewną wadą użycia metody Aitkena jest to, że nie wiadomo, który z wielu otrzymanych wyników jest najdokładniejszy. Przewagę ma tutaj użycie stablicowanych współczynników, które daje dużą poprawę dokładności, gdy początkowy wynik jest już dość dokładny. Jeśli początkowy wynik nie jest wystarczająco dokładny, również można użyć stablicowanych współczynników, zmieniając wartość $n$ na wyższą, gdyż wyznaczanie kwadratury z stablicowanymi współczynnikami dla dużych n jest dokładne jest dokładne z powododu minimalnych błędów numerycznych.


\newpage
\thispagestyle{empty}
\begin{thebibliography}{99}
 \bibitem{} S. Paszkowski  \textit{Zastosowania numeryczne wielomianów i szeregów Czebyszewa}, PWN, 1975
 \bibitem{} M. Kamermans \textit{Gaussian Quadrature Weights and Abscissae}, http://pomax.github.io/bezierinfo/legendre-gauss.html
 \bibitem{} M.Abramowitz, I.Stegun  \textit{Handbook of Mathematical Functions}, Dover, 1972, 25.4.38
\end{thebibliography}
\end{document}

