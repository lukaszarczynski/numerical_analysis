// Łukasz Arczyński
// nr. indeksu 258424
// zad P3.4
// Wrocław, 24 I 2014 r.

#include <iostream>
#include <cmath>
#include <iomanip>
#include "polynomial.h"

using namespace std;
const double PI = 3.14159265358979323846;

///// Wagi i miejsca zerowe x_i uzywane w kwadraturze  Gaussa-Legendre'a /////

const double ZEROES_OF_LEGENDRE_POLYNOMIAL_DEG_8[] = {-0.960289856497536, -0.796666477413626, -0.525532409916329,
                                                      -0.183434642495649,  0.183434642495649,  0.525532409916329,
                                                       0.796666477413626,  0.960289856497536};
const double LEGENDRE_QUADRATURE_WEIGHTS_DEG_8[] = {0.101228536290376, 0.222381034453374, 0.313706645877887,
                                                    0.362683783378362, 0.362683783378362, 0.313706645877887,
                                                    0.222381034453374, 0.101228536290376};
const double ZEROES_OF_LEGENDRE_POLYNOMIAL_DEG_21[] = {-0.993752170620389, -0.967226838566306, -0.920099334150400,
                                                       -0.853363364583317, -0.768439963475677, -0.667138804197412,
                                                       -0.551618835887219, -0.424342120207438, -0.288021316802401,
                                                       -0.145561854160895,  0.000000000000000,  0.145561854160895,
                                                        0.288021316802401,  0.424342120207438,  0.551618835887219,
                                                        0.667138804197412,  0.768439963475677,  0.853363364583317,
                                                        0.920099334150400,  0.967226838566306,  0.993752170620389};
const double LEGENDRE_QUADRATURE_WEIGHTS_DEG_21[] = {0.016017228257774, 0.036953789770853, 0.057134425426857,
                                                     0.076100113628379, 0.093444423456034, 0.108797299167148,
                                                     0.121831416053728, 0.132268938633337, 0.139887394791073,
                                                     0.144524403989970, 0.146081133649690, 0.144524403989970,
                                                     0.139887394791073, 0.132268938633337, 0.121831416053728,
                                                     0.108797299167148, 0.093444423456034, 0.076100113628379,
                                                     0.057134425426857, 0.036953789770853, 0.016017228257774};


///// Wyznaczanie wartosci potrzebnych do obliczania wspolczynnikow kwadratur interpolacyjnych /////

Polynomial omega(int numberOfNodes, double *nodes, int nodeIndex) //omega(x)/(x-x_k) dla k>=0; omega(x) wpp.
{
    Polynomial monomial;
    double coefficients[2];
    coefficients[1] = 1;
    double one[] = {1.0};
    Polynomial result(0, one);
    for (int i = 0; i <= numberOfNodes; i++)
    {
        coefficients[0] = -nodes[i];
        monomial = Polynomial(1, coefficients);
        if (i != nodeIndex)
            result *= monomial;
    }
    return result;
}

double derivativeOfOmega(int numberOfNodes, double *nodes, int nodeIndex) // omega'(x_k)
{
    double result = 1.0;
    for (int i = 0; i <= numberOfNodes; i++)
        if (i != nodeIndex)
            result *= nodes[nodeIndex] - nodes[i];
    return result;
}

double integralOfOmega(int numberOfNodes, double *nodes, int nodeIndex)
{
    return omega(numberOfNodes, nodes, nodeIndex).integral(-1.0, 1.0);
}

double A_k(int numberOfNodes, double nodes[], int nodeIndex) // wagi kwadratury interpolacyjnej
{
    Polynomial I = omega(numberOfNodes, nodes, nodeIndex);
    return 1 / derivativeOfOmega(numberOfNodes, nodes, nodeIndex) * I.integral(-1.0, 1.0);
}

///// Przyblizanie calki za pomoca kwadratury interpolacyjnej - wzor ogolny /////

double f(double x, int selectedFunctionNumber) // funkcja podcalkowa
{

    switch (selectedFunctionNumber)
    {
        case -1:
            return 1;
        case 0:
            return sin(x);
        case 1:
            return 1.0 / (pow(x, 4) + pow(x, 2) + 0.9);
        case 2:
            return 4.0 / (PI * (1.0 + pow(x, 2)));
        case 3:
            return (2.0 * PI * PI * (1.0 + x)) / ((1 - x) * (3 + x));
        case 4:
            return pow(2.71828182845904523536, -x * x);
        case 5:
            return 1 / (1 + 25 * x * x);
        case 6:
            return pow(x, 73);
        case 7:
            return 1 / pow(1 - pow(x, 2), 0.5);
        default:
            return 0;
    }
}

double quadrature(int numberOfNodes, double *nodes, int selectedFunctionNumber) // kwadratura interpolacyjna - ogolny wzor
{
    double result = 0.0;
    for (int nodeIndex = 0; nodeIndex <= numberOfNodes; nodeIndex++)
        result += A_k(numberOfNodes, nodes, nodeIndex) * f(nodes[nodeIndex], selectedFunctionNumber);
    return result;
}

///// Przyblizanie calki za pomoca kwadratury Newtona-Cotesa /////

long long int factorial(int n)
{
    long long int result = 1.0;
    for (int i = 1; i <= n; i++)
        result *= i;
    return result;
}

Polynomial newtonCotesQuotient(int n, int k) // iloraz x(x-1)...(x-n)/(x-k)
{
    Polynomial monomial;
    double coefficients[2];
    coefficients[1] = 1;
    double one[] = {1.0};
    Polynomial result(0, one);
    for (int i = 0; i <= n; i++)
    {
        coefficients[0] = -i;
        monomial = Polynomial(1, coefficients);
        if (i != k)
            result *= monomial;
    }
    return result;
}

double newtonCotesQuadrature(int numberOfNodes, double stepSize, double *nodes, int selectedFunctionNumber)
// kwadratura Newtona-Cotesa - prostszy wzor
{
    double result = 0.0;
    for (int nodeIndex = 0; nodeIndex <= numberOfNodes; nodeIndex++)
        result += pow(-1, numberOfNodes - nodeIndex) * newtonCotesQuotient(numberOfNodes, nodeIndex).integral(0, numberOfNodes) /
                  factorial(nodeIndex) * stepSize / factorial(numberOfNodes - nodeIndex) * f(nodes[nodeIndex], selectedFunctionNumber);
    return result;
}

///// Tworzenie wielomianow ortogonalnych zdefiniowanych rekurencyjnie /////

void polynomialsWithConstantRecursiveEquation(int degree, double *A, double *B, double *C, double A0, double B0,
                                              double Ak, double Bk, double Ck)
// Gdy wspolczynniki A, B, C stosowane we wzorze rekurencyjnym sa stale
{
    A[0] = A0;
    B[0] = B0;
    for (int i = 1; i < degree + 1; i++)
    {
        A[i] = Ak, B[i] = Bk;
        C[i] = Ck;
    }
}

void legendrePolynomials(int degree, double *A, double *B, double *C)
// Wspolczynniki A, B, C uzywane do rekurencyjnego wyznaczania wielomianow Legendre'a
{
    A[0] = 1;
    B[0] = 0;
    for (int i = 1.0; i < degree + 1; i++)
    {
        A[i] = (2.0 * i + 1.0) / (i + 1.0), B[i] = 0;
        C[i] = i / (i + 1.0);
    }
}

void orthogonalPolynomials(int degree, Polynomial *w, double *A, double *B, double *C)
// Obliczanie kolejnych wielomianow ortogonalnych zdefiniowanych rekurencyjnie
{
    double w0[] = {1};
    double x[] = {0, 1};
    Polynomial X(1, x);
    w[0] = Polynomial(0, w0);
    w[1] = (A[0] * X * w[0]) + B[0] * w[0];
    for (int i = 1; i < degree; i++)
        w[i + 1] = (A[i] * X * w[i]) + B[i] * w[i] - C[i] * w[i - 1];
}

///// Przyblizanie calki za pomoca kwadratury Gaussa-Czebyszewa /////

void chebyshevPolynomials(int degree, Polynomial *q)
{
    double A[degree + 2], B[degree + 2], C[degree + 2];
    polynomialsWithConstantRecursiveEquation(degree + 1, A, B, C, 1, 0, 2, 0, 1);
    orthogonalPolynomials(degree + 1, q, A, B, C);
}

double chebyshevQuadrature(int numberOfNodes, double *nodes, int selectedFunctionNumber) // kwadratura Gaussa-Czebyszewa - prostszy wzor
{
    Polynomial chebyshev[numberOfNodes + 2];
    chebyshevPolynomials(numberOfNodes, chebyshev);
    double result = 0.0;
    for (int nodeIndex = 0; nodeIndex <= numberOfNodes; nodeIndex++)
        result += PI / (numberOfNodes + 1) * f(nodes[nodeIndex], selectedFunctionNumber) * pow(1 - pow(nodes[nodeIndex], 2), 0.5);
    return result;
}

///// Przyblizanie calki za pomoca kwadratury Gaussa-Legendre'a /////

double legendreQuadrature(int numberOfNodes, double *nodes, int selectedFunctionNumber)
{
    double weights[numberOfNodes];
    if (numberOfNodes == 8)
        for (int i = 0; i < numberOfNodes; i++)
            weights[i] = LEGENDRE_QUADRATURE_WEIGHTS_DEG_8[i];
    else
        for (int i = 0; i < numberOfNodes; i++)
            weights[i] = LEGENDRE_QUADRATURE_WEIGHTS_DEG_21[i];
    double result = 0.0;
    for (int k = 0; k < numberOfNodes; k++)
        result += f(nodes[k], selectedFunctionNumber) * weights[k];
    return result;
}


int main() /// 1,8,2 -> -log bledu = 3.14
{
    int nodesDistribution = -2;
    while (nodesDistribution != 8)
    {
        int numberOfNodes = 8, selectedFunctionNumber;
        double zero = 0.0;
        double approximateResult;
        double stepSize = 0;
        double nodes[numberOfNodes];
        double exactResults[] = {0.0, 1.582232963729673, 2.0, 1 / zero, 1.493648265624854,
                                 0.5493603067780066, 0.0, PI,};
        cout << "Wybierz rozmieszczenie wezlow:\n1. rownoodlegle\n2. rownoodlegle (uproszczony wzor)\n"
                "3. zera (n+1)-go wielomianu Czebyszewa\n4. zera (n+1)-go wielomianu Czebyszewa (z funkcja wagowa 1/(sqrt(1-x^2)))\n"
                "5. punkty ekstremalne n-tego wielomianu Czebyszewa\n6. zera n-tego wielomianu Legendre'a - wyznaczanie wspolczynnikow \n(tylko dla n=8)\n"
                "7. zera n-tego wielomianu Legendre'a - wspolczynniki stablicowane \n(tylko dla n=8,21)\n8. Koniec\n";
        cin >> nodesDistribution;
        if (nodesDistribution != 6 && nodesDistribution != 8)
        {
            cout << "Wybierz liczbe wezlow\n";
            cin >> numberOfNodes;
        }
        if (nodesDistribution != 8)
        {
            cout << "Wybierz funkcje:\n0. sin(x)\n1. 1/(x^4+x^2+0.9)\n2. 4/Pi(1+x^2)\n"
                    "3. 2Pi^2(1+x)/((1-x)(3+x))\n 4. e^-x^2\n5. 1/(1+25x^2)\n6. x^73\n7. 1/sqrt(1-x^2)\n";
            cin >> selectedFunctionNumber;
            stepSize = 2.0 / numberOfNodes;
        }
        switch (nodesDistribution)
        {
            case 1:
                for (int i = 0.0; i <= numberOfNodes; i++)
                    nodes[i] = -1.0 + i * stepSize;
                approximateResult = quadrature(numberOfNodes, nodes, selectedFunctionNumber);
                break;
            case 2:
                for (int i = 0.0; i <= numberOfNodes; i++)
                    nodes[i] = -1.0 + i * stepSize;
                approximateResult = newtonCotesQuadrature(numberOfNodes, stepSize, nodes, selectedFunctionNumber);
                break;
            case 3:
                for (int i = 0; i <= numberOfNodes; i++)
                    nodes[i] = cos((2.0 * i + 1) / (2.0 * (numberOfNodes + 1.0)) * PI);
                approximateResult = quadrature(numberOfNodes, nodes, selectedFunctionNumber);
                break;
            case 4:
                for (int i = 0; i <= numberOfNodes; i++)
                    nodes[i] = cos((2.0 * i + 1) / (2.0 * (numberOfNodes + 1.0)) * PI);
                approximateResult = chebyshevQuadrature(numberOfNodes, nodes, selectedFunctionNumber);
                break;
            case 5:
                for (int i = 0.0; i <= numberOfNodes; i++)
                    nodes[i] = cos((double) i / (double) numberOfNodes * PI);
                approximateResult = quadrature(numberOfNodes, nodes, selectedFunctionNumber);
                break;
            case 6:
                for (int i = 0.0; i <= numberOfNodes; i++)
                    nodes[i] = ZEROES_OF_LEGENDRE_POLYNOMIAL_DEG_8[i];
                approximateResult = quadrature(numberOfNodes, nodes, selectedFunctionNumber);
                break;
            case 7:
                if (numberOfNodes <= 14)
                {
                    numberOfNodes = 8;
                    cout << "\nn = 8\n";
                }
                else
                {
                    numberOfNodes = 21;
                    cout << "\nn = 21\n";
                }
                if (numberOfNodes == 8)
                    for (int i = 0.0; i <= numberOfNodes; i++)
                        nodes[i] = ZEROES_OF_LEGENDRE_POLYNOMIAL_DEG_8[i];
                if (numberOfNodes == 21)
                    for (int i = 0.0; i <= numberOfNodes; i++)
                        nodes[i] = ZEROES_OF_LEGENDRE_POLYNOMIAL_DEG_21[i];
                approximateResult = legendreQuadrature(numberOfNodes, nodes, selectedFunctionNumber);
                break;
            case 8:
                return 0;
            default:
                return 0;
        }
        cout << "\nwynik: " << setprecision(15) << approximateResult;
        cout << "\nblad: " << setprecision(8) << abs(approximateResult - exactResults[selectedFunctionNumber]);
        cout << "\n-log_10 bledu: " << fixed << setprecision(3) <<
        -log10(abs(approximateResult - exactResults[selectedFunctionNumber])) << "\n\n\n";
    }
    return 0;
}